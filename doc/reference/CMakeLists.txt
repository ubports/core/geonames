find_package(GtkDoc 1.33 REQUIRED)

configure_file(geonames-docs.xml.in "${CMAKE_BINARY_DIR}/doc/geonames-docs.xml" @ONLY)

gtk_doc_add_module(${PROJECT_NAME}
    XML "${CMAKE_BINARY_DIR}/doc/geonames-docs.xml"
    SOURCE "${CMAKE_SOURCE_DIR}/src"
    SUFFIXES "h" "c"

    LIBRARIES geonames

    IGNOREHEADERS geonames-query.h
)

add_custom_target(docs ALL
    DEPENDS ${PROJECT_NAME}
)
add_dependencies(docs doc-${PROJECT_NAME})
add_dependencies(doc-${PROJECT_NAME} ${PROJECT_NAME})

install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html/ DESTINATION ${CMAKE_INSTALL_DATADIR}/gtk-doc/html/${PROJECT_NAME})
