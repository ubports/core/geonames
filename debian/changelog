geonames (0.3.1) unstable; urgency=medium

  * Upstream-provided Debian package for geonames. See upstream
    ChangeLog for recent changes.

 -- UBports developers <developers@ubports.com>  Thu, 25 Jan 2024 16:43:17 +0100

geonames (0.3.0) unstable; urgency=medium

  [ Marius Gripsgard ]
  * New bump version
  * Switch to cmake

  [ UBports developers ]
  * Upstream-provided Debian package for geonames. See upstream
    ChangeLog for recent changes.

 -- UBports developers <developers@ubports.com>  Wed, 04 Jan 2023 07:37:45 +0100

geonames (0.2.0) focal; urgency=medium

  * Build for focal.

 -- Marius Gripsgard <marius@ubports.com>  Thu, 14 Jul 2022 04:44:07 +0200

geonames (0.2+17.04.20170220-0ubuntu2) disco; urgency=medium

  * Use correct machine-readable copyright file URI.
  * Use secure URI in Vcs control header.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Jan 2019 02:58:48 +0000

geonames (0.2+17.04.20170220-0ubuntu1) zesty; urgency=medium

  * Be less strict on city search (LP: #1590462)

 -- Albert Astals Cid <albert.astals@canonical.com>  Mon, 20 Feb 2017 11:07:52 +0000

geonames (0.2+16.04.20160426-0ubuntu1) xenial; urgency=medium

  * Add support for translations of city, states, and country names.

 -- Michael Terry <michael.terry@canonical.com>  Tue, 26 Apr 2016 08:44:43 +0000

geonames (0.2+16.04.20160321-0ubuntu1) xenial; urgency=medium

  [ Michael Terry ]
  * Add geonames_city_get_country_code, geonames_city_get_latitude,
    geonames_city_get_longitude, and geonames_city_get_population

  [ CI Train Bot ]
  * debian/libgeonames0.symbols: update to released version.
  * No-change rebuild.

 -- Michael Zanetti <michael.zanetti@canonical.com>  Mon, 21 Mar 2016 23:47:04 +0000

geonames (0.1+16.04.20160315-0ubuntu1) xenial; urgency=medium

  [ Lars Uebernickel ]
  * Add demo application added: demo/ demo/Makefile.am demo/geonames-
    demo.c
  * Add documentation added: doc/ doc/Makefile.am doc/reference/
    doc/reference/Makefile.am doc/reference/geonames-docs.xml.in
  * Add geonames_get_n_cities()
  * Add geonames_query_cities_sync()
  * Add pkgconfig file added: src/geonames.pc.in
  * Add sanity test for empty results
  * Add support for g_autoptr
  * Don't install geonames-mkdb
  * Empty argument list is bad C99
  * Ensure that geonames db is loaded before querying it
  * Favor larger cities
  * Fix typo in public enum field
  * Install headers
  * demo: don't leak indices
  * query_cities: return array directly
  * query_cities_finish: write to indices out parameter
  * tests: link against libgeonames and use new sync api

  [ Michael Terry ]
  * Fix double-free when querying.
  * Merge debian packaging branch added: COPYING debian/
    debian/changelog debian/compat debian/control debian/copyright
    debian/libgeonames-dev.install debian/libgeonames0.install
    debian/libgeonames0.symbols debian/rules debian/source/
    debian/source/format

 -- Ken VanDine <ken.vandine@canonical.com>  Tue, 15 Mar 2016 13:33:03 +0000

geonames (0.1-0ubuntu1) xenial; urgency=low

  * Initial release

 -- Lars Uebernickel <lars.uebernickel@ubuntu.com>  Thu, 17 Sep 2015 17:04:21 +0200
