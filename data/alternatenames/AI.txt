11840362	3573364	link	https://en.wikipedia.org/wiki/West_End,_Anguilla						
1454104	3573365		West End Pond						
4307439	3573365		West End Salt Pond						
16709626	3573365	link	https://en.wikipedia.org/wiki/West_End_Pond						
1454105	3573368		Welches						
4307440	3573368		Welches Hill						
1454106	3573371	en	Wallblake Airport						
1892997	3573371	icao	TQPF						
1892998	3573371	iata	AXA						
2986035	3573371	link	https://en.wikipedia.org/wiki/Clayton_J._Lloyd_International_Airport						
8065629	3573371	en	Anguilla Wallblake Airport				1		
8065630	3573371	en	Clayton J. Lloyd International Airport	1					
8067563	3573371	fr	Aéroport international Clayton J. Lloyd						
11549049	3573371	fr	Aéroport de Wallblake						
11549050	3573371	it	Aeroporto di The Valley-Wallblake						
11549051	3573371	lt	Angilijos Wallblake oro uostas						
15501715	3573371	wkdt	Q1657604						
1454107	3573373		Maunday’s Point						
4307441	3573373		Upper Maunday’s Point						
1454108	3573374		Valley						
1649402	3573374	fr	The Valley						
1649480	3573374	da	The Valley						
1649555	3573374	el	Δε Βάλεϊ						
1649764	3573374	fi	The Valley						
1994381	3573374	es	El Valle						
1994382	3573374	pl	The Valley						
1994383	3573374	de	The Valley						
1994384	3573374	en	The Valley						
1994385	3573374	he	הוואלי						
1994386	3573374	id	The Valley						
1994387	3573374	io	The Valley						
1994388	3573374	it	The Valley						
1994389	3573374	ja	バレー						
1994390	3573374	ko	더밸리						
1994391	3573374	nl	The Valley						
1994392	3573374	pt	The Valley						
1994393	3573374	sk	The Valley						
1994394	3573374	sv	The Valley						
2986036	3573374	link	https://en.wikipedia.org/wiki/The_Valley%2C_Anguilla						
3043356	3573374	ru	Валли						
9368235	3573374	be	Валі						
9368236	3573374	bg	Вали						
9368237	3573374	eo	La-Valo						
9368238	3573374	fa	د ولی						
9368239	3573374	ka	ვალი						
9368240	3573374	lt	Valis						
9368241	3573374	sr	Вали						
9368242	3573374	th	เดอะแวลลีย์						
9368243	3573374	uk	Валлі						
9368244	3573374	ur	دی ویلی						
9368245	3573374	zh	瓦利						
11344675	3573374	ky	Валли						
11344676	3573374	ta	தி வேல்லி						
13722507	3573374	unlc	AIVAL						
7198633	3573375	link	https://en.wikipedia.org/wiki/The_Quarter%2C_Anguilla						
7323569	3573375		The Quarter						
13722505	3573377	unlc	AIFOR						
13826836	3573377		The Forest						
7190098	3573378	link	https://en.wikipedia.org/wiki/The_Farrington						
7323570	3573378		The Farrington						
7193880	3573380	link	https://en.wikipedia.org/wiki/Stoney_Ground						
7323571	3573380		Stoney Ground						
11840360	3573385	link	https://en.wikipedia.org/wiki/South_Hill,_Anguilla						
1454109	3573387		Hat Island						
1454110	3573387		Sombrero Island						
2986037	3573387	link	https://en.wikipedia.org/wiki/Sombrero%2C_Anguilla						
4307442	3573387		Sombrero						
13722506	3573387	unlc	AISOM						
1454111	3573390		Shoal Village						
4307443	3573390		Shoal Bay Village						
1454112	3573394		Lovers Point						
4307444	3573394		Shaddick Point						
1454113	3573396		Seal Islets						
1454114	3573396		Seal Islands						
4307445	3573396		Seal Island						
6889690	3573396	link	https://en.wikipedia.org/wiki/Seal_Island%2C_Anguilla						
1454115	3573398		Scrub Islet						
4307446	3573398		Scrub Island						
7135372	3573398	link	https://en.wikipedia.org/wiki/Scrub_Island%2C_Anguilla						
1454116	3573405		Sand Island						
4307447	3573405		Sandy Island						
16140810	3573405	link	https://en.wikipedia.org/wiki/Sandy_Island%2C_Anguilla						
7142735	3573406	link	https://en.wikipedia.org/wiki/Sandy_Hill%2C_Anguilla						
7323572	3573406		Sandy Hill Bay						
6922966	3573407		Sandy Ground Village						
16241584	3573407	link	https://en.wikipedia.org/wiki/Sandy_Ground,_Anguilla						
1454117	3573409		Salt Pond						
4307448	3573409		Road Salt Pond						
11316615	3573409	link	https://en.wikipedia.org/wiki/Road_Salt_Pond						
1454118	3573413		Rendezvous Salt Pond						
1454119	3573413		Rendezvous Pond						
4307449	3573413		Rendezvous Bay Salt Pond						
16709625	3573413	link	https://en.wikipedia.org/wiki/Rendezvous_Bay_Pond						
1454120	3573415		Prickley Pear Cays						
4307450	3573415		Prickly Pear Cays						
7138271	3573415	link	https://en.wikipedia.org/wiki/Prickly_Pear_Cays						
7202442	3573418	link	https://en.wikipedia.org/wiki/North_Side%2C_Anguilla						
7323573	3573418		North Side						
1454121	3573419		Shannon Hill						
4307451	3573419		North Shannon Hill						
11840356	3573420	link	https://en.wikipedia.org/wiki/North_Hill,_Anguilla						
16709623	3573426	link	https://en.wikipedia.org/wiki/Meads_Bay_Pond						
16712369	3573426		Mead’s Bay Pond						
1454122	3573433		Shoal Bay						
4307452	3573433		Lower Shoal Bay						
1454123	3573435		Long Pond						
4307453	3573435		Long Salt Pond						
8067933	3573435	link	https://en.wikipedia.org/wiki/Long_Pond_%28Anguilla%29						
16709624	3573441	link	https://en.wikipedia.org/wiki/Long_Pond,_Anguilla						
16712370	3573441		Long Bay Pond						
1454124	3573444		Little Scrub Islet						
4307454	3573444		Little Scrub Island						
1454125	3573447		Little Dicks						
4307455	3573447		Little Dix						
1454126	3573452		Katouche Bay						
4307456	3573452		Latouche Bay						
7995937	3573456	link	https://en.wikipedia.org/wiki/Island_Harbour,_Anguilla						
1454127	3573459		Maunday’s Bay Pond						
4307457	3573459		Gull Pond						
8095150	3573460	link	https://en.wikipedia.org/wiki/Grey_Pond						
8108673	3573460		Grey Pond						
7162217	3573466	link	https://en.wikipedia.org/wiki/George_Hill%2C_Anguilla						
7323574	3573466		George Hill						
16709622	3573468	link	https://en.wikipedia.org/wiki/Forest_Bay_Pond						
16712371	3573468		Forest Bay						
7507837	3573469	link	https://en.wikipedia.org/wiki/Flirt_Rocks						
7566288	3573469		Flirt Rocks						
2060	3573473		East End						
4307458	3573473		East End Village						
7512037	3573473	link	https://en.wikipedia.org/wiki/East_End%2C_Anguilla						
1454128	3573475		Dowlings Shoal						
1454129	3573475		Dowling						
4307459	3573475		Dowling Shoal						
1454130	3573476		Dog Islet						
4307460	3573476		Dog Island						
7197386	3573476	link	https://en.wikipedia.org/wiki/Dog_Island%2C_Anguilla						
1454131	3573480		Saint Mary’s						
4307461	3573480		Crocus Hill						
13722504	3573480	unlc	AICRH						
8084962	3573482	link	https://en.wikipedia.org/wiki/Cove_Pond						
8108674	3573482		Cove Pond						
11313545	3573488	link	https://en.wikipedia.org/wiki/Cauls_Pond						
11392365	3573488		Cauls Pond						
7995938	3573496	link	https://en.wikipedia.org/wiki/Blowing_Point,_Anguilla						
8005032	3573496		Blowing Point Village						
13722503	3573496	unlc	AIBLP						
1454132	3573504		Bendig Bay						
4307462	3573504		Benzies Bay						
1454133	3573508		Badcocks Pond						
4307463	3573508		Bad Cox Pond						
1627089	3573511	ca	Anguilla	1					
2187917	3573511	es	Anguila	1					
2420930	3573511	am	አንጉኢላ	1					
2420931	3573511	ar	أنغويلا	1					
2420932	3573511	be	Ангуілля	1					
2420933	3573511	bg	Ангуила	1					
2420934	3573511	bn	এ্যাঙ্গুইলা	1					
2420935	3573511	bo	ཨང་གུའི་ལ།	1					
2420936	3573511	cs	Anguilla	1					
2420937	3573511	cy	Anguilla	1					
2420938	3573511	da	Anguilla	1					
2420939	3573511	de	Anguilla	1					
2420940	3573511	el	Ανγκουίλα	1					
2420941	3573511	en	Anguilla	1					
2420942	3573511	eo	Angvilo	1					
2420943	3573511	et	Anguilla	1					
2420944	3573511	fa	آنگیل	1					
2420945	3573511	fi	Anguilla	1					
2420946	3573511	fr	Anguilla	1					
2420947	3573511	ga	Anguilla	1					
2420948	3573511	he	אנגילה	1					
2420949	3573511	hi	एंगुइला	1					
2420950	3573511	hr	Anguila	1					
2420951	3573511	hu	Anguilla	1					
2420952	3573511	id	Anguilla	1					
2420953	3573511	is	Angvilla	1					
2420954	3573511	it	Anguilla	1					
2420955	3573511	ja	アンギラ	1					
2420956	3573511	ka	ანგვილა	1					
2420957	3573511	ko	안길라						
2420958	3573511	lo	ອັນກິລາ	1					
2420959	3573511	lt	Angilija	1					
2420960	3573511	lv	Angilja	1					
2420961	3573511	mk	Ангвила	1					
2420962	3573511	ms	Anguilla	1					
2420963	3573511	mt	Angwilla	1					
2420964	3573511	nb	Anguilla	1					
2420965	3573511	nl	Anguilla	1					
2420966	3573511	nn	Anguilla	1					
2420967	3573511	pl	Anguilla	1					
2420968	3573511	pt	Anguilla	1					
2420969	3573511	ro	Anguilla	1					
2420970	3573511	ru	Ангилья	1					
2420971	3573511	se	Anguilla	1					
2420972	3573511	sk	Anguilla	1					
2420973	3573511	sl	Angvila	1					
2420974	3573511	sr	Ангвила	1					
2420975	3573511	sv	Anguilla	1					
2420976	3573511	th	แองกวิลลา	1					
2420977	3573511	tr	Anguilla	1					
2420978	3573511	uk	Ангілья	1					
2420979	3573511	zh	安圭拉	1					
4307464	3573511		Anguilla						
5423944	3573511	link	https://en.wikipedia.org/wiki/Anguilla						
7084493	3573511	af	Anguilla	1					
7084494	3573511	ak	Anguila	1					
7084495	3573511	az	Anquila	1					
7084496	3573511	bm	Angiya	1					
7084497	3573511	br	Anguilla	1					
7084498	3573511	bs	Angvila	1					
7084499	3573511	ee	Anguilla nutome	1					
7084500	3573511	eu	Angila	1					
7084501	3573511	ff	Anngiyaa	1					
7084502	3573511	fo	Anguilla	1					
7084503	3573511	gl	Anguila	1					
7084504	3573511	gu	ઍંગ્વિલા	1					
7084505	3573511	ha	Angila	1					
7084506	3573511	ki	Anguilla	1					
7084507	3573511	kn	ಆಂಗುಯಿಲ್ಲಾ	1					
7084508	3573511	lg	Angwila	1					
7084509	3573511	ln	Angiyɛ	1					
7084510	3573511	lu	Angiye	1					
7084511	3573511	mg	Anguilla	1					
7084512	3573511	ml	ആന്‍ഗ്വില്ല	1					
7084513	3573511	mr	अँग्विला	1					
7084514	3573511	my	အန်ကွီလာ	1					
7084515	3573511	nd	Anguilla	1					
7084516	3573511	ne	आङ्गुइला	1					
7084517	3573511	oc	Anguilla	1					
7084518	3573511	or	ଆଙ୍ଗୁଇଲ୍ଲା	1					
7084519	3573511	rm	Anguilla	1					
7084520	3573511	rn	Angwila	1					
7084521	3573511	sg	Angûîla	1					
7084522	3573511	si	ඇන්ගුයිලාව	1					
7084523	3573511	sn	Anguila	1					
7084524	3573511	so	Anguilla	1					
7084525	3573511	sw	Anguilla	1					
7084526	3573511	ta	அங்குய்லா	1					
7084527	3573511	te	ఆంగవిల్లా	1					
7084528	3573511	ti	አንጉኢላ	1					
7084529	3573511	to	Anikuila	1					
7084530	3573511	ur	انگوئیلا	1					
7084531	3573511	vi	Anguilla	1					
7084532	3573511	yo	Orílẹ́ède Ààngúlílà	1					
7084533	3573511	zu	i-Anguilla	1					
13337906	3573511	ko	앵귈라	1					
13722502	3573511	unlc	AIAXA						
16784151	3573511	uz	Angilya	1					
16925657	3573511	zh-Hant	安奎拉	1					
16926028	3573511	am	አንጉይላ	1					
16926029	3573511	as	এনগুইলা	1					
16926030	3573511	az	Angilya	1					
16926031	3573511	be	Ангілья	1					
16926032	3573511	ce	Ангилья	1					
16926033	3573511	dz	ཨང་གི་ལ	1					
16926034	3573511	eu	Aingira	1					
16926035	3573511	fa	آنگویلا	1					
16926036	3573511	fy	Anguilla	1					
16926037	3573511	ga	Angaíle	1					
16926038	3573511	gd	Anguillia	1					
16926039	3573511	he	אנגווילה	1					
16926040	3573511	hi	एंग्विला	1					
16926041	3573511	hr	Angvila	1					
16926042	3573511	hy	Անգուիլա	1					
16926043	3573511	ig	Anguilla	1					
16926044	3573511	jv	Anguilla	1					
16926045	3573511	ka	ანგილია	1					
16926046	3573511	kk	Ангилья	1					
16926047	3573511	kl	Anguilla	1					
16926048	3573511	km	អង់ហ្គីឡា	1					
16926049	3573511	kn	ಆಂಗ್ವಿಲ್ಲಾ	1					
16926050	3573511	ks	انگوئیلا	1					
16926051	3573511	ky	Ангилья	1					
16926052	3573511	lb	Anguilla	1					
16926053	3573511	lo	ແອນກຸຍລາ	1					
16926054	3573511	ml	ആൻഗ്വില്ല	1					
16926055	3573511	mn	Ангилья	1					
16926056	3573511	mt	Anguilla	1					
16926057	3573511	my	အန်ဂီလာ	1					
16926058	3573511	no	Anguilla	1					
16926059	3573511	pa	ਅੰਗੁਇਲਾ	1					
16926060	3573511	ps	انګیلا	1					
16926061	3573511	pt	Anguila	1					
16926062	3573511	qu	Anguila	1					
16926063	3573511	sd	انگويلا	1					
16926064	3573511	so	Anguula	1					
16926065	3573511	sq	Anguilë	1					
16926066	3573511	ta	அங்கியுலா	1					
16926067	3573511	te	ఆంగ్విల్లా	1					
16926068	3573511	tg	Ангилия	1					
16926069	3573511	ti	ኣንጊላ	1					
16926070	3573511	tk	Angilýa	1					
16926071	3573511	tt	Ангилья	1					
16926072	3573511	ug	ئانگۋىللا	1					
16926073	3573511	wo	Angiiy	1					
16926074	3573511	yo	Ààngúlílà	1					
1620432	3573512	en	Anguilla						
1620433	3573512	de	Anguilla						
1620434	3573512	es	Anguila						
1620435	3573512	ca	Anguilla						
1620436	3573512	cs	Anguilla						
1620437	3573512	el	Ανγκουίλα						
1620438	3573512	eo	Angvilo						
1620439	3573512	et	Anguilla						
1620440	3573512	fi	Anguilla						
1620441	3573512	fr	Anguilla						
1620442	3573512	gl	Anguila - Anguilla						
1620443	3573512	he	אנגווילה						
1620444	3573512	hu	Anguilla						
1620445	3573512	id	Anguilla						
1620446	3573512	io	Anguila						
1620447	3573512	is	Angvilla						
1620448	3573512	it	Anguilla						
1620449	3573512	ja	アンギラ						
1620450	3573512	ko	앵귈라						
1620451	3573512	kw	Angwilla						
1620452	3573512	lt	Angilija						
1620453	3573512	lv	Angiļa						
1620454	3573512	nds	Anguilla						
1620455	3573512	nl	Anguilla						
1620456	3573512	no	Anguilla						
1620457	3573512	pl	Anguilla						
1620458	3573512	pt	Anguilla						
1620459	3573512	ro	Anguilla						
1620460	3573512	ru	Ангилья						
1620461	3573512	hbs	Angvila						
1620462	3573512	sl	Angvila						
1620463	3573512	sr	Ангиља						
1620464	3573512	sv	Anguilla						
1620465	3573512	tr	Anguilla						
1620466	3573512	uk	Ангілья						
1620467	3573512	zh	安圭拉						
1633421	3573512	da	Anguilla						
1633422	3573512	hr	Angvila						
1633423	3573512	sk	Anguilla						
1896196	3573512	sr	Ангвила						
1896197	3573512	uk	Анґілья						
2427751	3573512	vi	Anguilla						
2427851	3573512	th	แองกวิลลา						
4307465	3573512		Anguilla						
8066769	3573512	iata	AXA						
7515254	6511674	link	https://en.wikipedia.org/wiki/Cap_Juluca_Hotel						
16184065	10280552	link	https://en.wikipedia.org/wiki/Ronald_Webster_Park						
11840355	11205389	link	https://en.wikipedia.org/wiki/Blowing_Point,_Anguilla						
11840410	11205392	link	https://en.wikipedia.org/wiki/Sandy_Ground%2C_Anguilla						
11840359	11205393	link	https://en.wikipedia.org/wiki/Sandy_Hill,_Anguilla						
11840361	11205396	link	https://en.wikipedia.org/wiki/The_Valley,_Anguilla						
11840411	11205433	link	https://en.wikipedia.org/wiki/East_End%2C_Anguilla						
11840401	11205436	link	https://en.wikipedia.org/wiki/North_Hill%2C_Anguilla						
11840399	11205437	link	https://en.wikipedia.org/wiki/West_End%2C_Anguilla						
11840400	11205438	link	https://en.wikipedia.org/wiki/South_Hill%2C_Anguilla						
11840402	11205439	link	https://en.wikipedia.org/wiki/The_Quarter,_Anguilla						
11840403	11205440	link	https://en.wikipedia.org/wiki/North_Side,_Anguilla						
11840404	11205441	link	https://en.wikipedia.org/wiki/Island_Harbour%2C_Anguilla						
11840412	11205442	link	https://en.wikipedia.org/wiki/George_Hill%2C_Anguilla						
11840405	11205443	link	https://en.wikipedia.org/wiki/Stoney_Ground						
11840406	11205444	link	https://en.wikipedia.org/wiki/The_Farrington						
