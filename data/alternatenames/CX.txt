15621679	2078071	wkdt	Q23017497						
15983226	2078071		Wharton Hill						
1024058	2078073		Waterfall						
4474259	2078073		The Waterfall						
15458268	2078079	wkdt	Q23017490						
15983227	2078079		Taits Vale						
15594667	2078080	wkdt	Q23017489						
15983228	2078080		Tait Point						
1024059	2078081		Sidney’s Dale						
4474260	2078081		Sydneys Dale						
14848611	2078082	wkdt	Q23017487						
15983229	2078082		Stubbings Point						
15812480	2078086	wkdt	Q23017483						
15983230	2078086		South Point						
15548124	2078090	wkdt	Q23017481						
15983231	2078090		Smith Point						
15401928	2078094	wkdt	Q23017478						
15983232	2078094		Ross Hill						
5902365	2078102	link	https://en.wikipedia.org/wiki/Murray_Hill%2C_Christmas_Island						
6141568	2078102		Murray Hill						
15555904	2078103	wkdt	Q23017470						
15983233	2078103		Middle Point						
15489822	2078104	wkdt	Q23017469						
15983234	2078104		Medwin Point						
15523281	2078106	wkdt	Q23017467						
15983235	2078106		Mc Micken Point						
8159482	2078113		John D Point						
8159483	2078113		John D. Point						
15626810	2078113	wkdt	Q23017460						
15446195	2078122	wkdt	Q23017451						
15983236	2078122		Grimes Cave						
15287211	2078123	wkdt	Q23017450						
15983237	2078123		Greta Beach						
1024060	2078127		The Settlement			1			
1649346	2078127	de	The Settlement			1			
1649376	2078127	en	The Settlement			1			
1649406	2078127	fr	The Settlement			1			
1649442	2078127	it	The Settlement			1			
1649491	2078127	da	The Settlement			1			
1649587	2078127	el	The Settlement			1			
1649730	2078127	es	Flying Fish Cove	1					
1649769	2078127	fi	The Settlement			1			
1649803	2078127	pt	The Settlement			1			
1649841	2078127	sv	The Settlement			1			
1922415	2078127	de	Flying Fish Cove	1					
1922416	2078127	pl	Flying Fish Cove	1					
1922417	2078127	fr	Flying Fish Cove	1					
1922418	2078127	en	Flying Fish Cove	1					
1922419	2078127	fi	Flying Fish Cove	1					
2939763	2078127	link	https://en.wikipedia.org/wiki/Flying_Fish_Cove						
8514142	2078127	post	6798						
11105925	2078127		Flying Fish Cove	1					
11461357	2078127	ar	فلاينغ فيش كوف						
11461358	2078127	az	Flayinq Fiş Kouv						
11461359	2078127	be	Флаін-Фіш-Коўв						
11461360	2078127	bg	Флаинг Фиш Коув						
11461361	2078127	hi	फ्लाइंग फिश कोव						
11461362	2078127	ja	フライング・フィッシュ・コーブ						
11461363	2078127	ka	ფლაინგ-ფიში						
11461364	2078127	ko	플라잉피시코브						
11461365	2078127	mk	Флаинг Фиш Коув						
11461366	2078127	pa	ਫ਼ਲਾਇੰਗ ਫ਼ਿਸ਼ ਕੋਵ						
11461367	2078127	sr	Флајинг Фиш Коув						
11461368	2078127	ta	பிளையிங் பிஷ் கோவ்						
11461369	2078127	th	ฟลายอิงฟิชโคฟ						
11461370	2078127	ur	فلائینگ فش کوو						
11461371	2078127	ru	Флайинг-Фиш-Коув						
11461372	2078127	zh	飞鱼湾						
13359478	2078127		Kampong						
13738433	2078127	unlc	CXFFC						
15579297	2078127	wkdt	Q30980						
15323771	2078132	wkdt	Q16541313						
16181280	2078132	link	https://en.wikipedia.org/wiki/Drumsite						
15789647	2078134	wkdt	Q23017442						
15983238	2078134		Dorothy Beach						
15613892	2078135	wkdt	Q23017441						
15983239	2078135		Dolly Beach						
2256609	2078138	fr	Territoire de l'Île Christmas						
2256610	2078138	fr	Île Christmas	1					
2296649	2078138	is	Jólaeyja						
2296699	2078138	is	Jólaeyjar						
2419701	2078138	am	ክሪስማስ ደሴት	1					
2419702	2078138	ar	جزيرة كريسماس	1					
2419703	2078138	be	Востраў Каляд	1					
2419704	2078138	bg	остров Рождество	1					
2419705	2078138	bn	ক্রিসমাস দ্বীপ	1					
2419706	2078138	ca	Illa Christmas	1					
2419707	2078138	cs	Vánoční ostrov	1					
2419708	2078138	cy	Ynys y Nadolig	1					
2419709	2078138	da	Juleøen	1					
2419710	2078138	de	Weihnachtsinsel	1					
2419711	2078138	el	Νήσος των Χριστουγέννων	1					
2419712	2078138	en	Christmas Island	1					
2419713	2078138	es	Isla de Navidad	1					
2419714	2078138	et	Jõulusaar	1					
2419715	2078138	eu	Christmas uhartea	1					
2419716	2078138	fa	جزیرهٔ کریسمس	1					
2419717	2078138	fi	Joulusaari	1					
2419718	2078138	fr	Christmas, île						
2419719	2078138	ga	Oileán na Nollag	1					
2419720	2078138	gl	Illa Christmas	1					
2419721	2078138	he	אי חג המולד	1					
2419722	2078138	hi	क्रिसमस द्वीप	1					
2419723	2078138	hr	Božićni Otok	1					
2419724	2078138	hu	Karácsony-sziget	1					
2419725	2078138	ia	Insula de Natal	1					
2419726	2078138	id	Pulau Natal	1					
2419727	2078138	it	Isola Christmas	1					
2419728	2078138	ja	クリスマス島	1					
2419729	2078138	ka	შობის კუნძული	1					
2419730	2078138	ko	크리스마스섬	1					
2419731	2078138	lt	Kalėdų sala	1					
2419732	2078138	lv	Ziemsvētku sala	1					
2419733	2078138	mk	Божиќен Остров	1					
2419734	2078138	ml	ക്രിസ്മസ് ദ്വീപ്	1					
2419735	2078138	ms	Pulau Krismas	1					
2419736	2078138	mt	il-Gżira Christmas	1					
2419737	2078138	nb	Christmasøya	1					
2419738	2078138	nl	Christmaseiland	1					
2419739	2078138	nn	Christmasøya	1					
2419740	2078138	pl	Wyspa Bożego Narodzenia	1					
2419741	2078138	pt	Ilha Christmas	1					
2419742	2078138	ro	Insula Christmas	1					
2419743	2078138	ru	о-в Рождества	1					
2419744	2078138	sk	Vianočný ostrov	1					
2419745	2078138	sl	Božični otok	1					
2419746	2078138	sr	Божићно Острво	1					
2419747	2078138	sv	Julön	1					
2419748	2078138	th	เกาะคริสต์มาส	1					
2419749	2078138	tr	Christmas Adası	1					
2419750	2078138	uk	Острів Різдва	1					
2419751	2078138	ur	جزیرہ کرسمس	1					
2419752	2078138	vi	Đảo Giáng Sinh	1					
2419753	2078138	zh	圣诞岛	1					
4474261	2078138		Territory of Christmas Island						
5423939	2078138	link	https://en.wikipedia.org/wiki/Christmas_Island						
7086100	2078138	af	Kerseiland	1					
7086101	2078138	az	Milad adası	1					
7086102	2078138	br	Enez Christmas	1					
7086103	2078138	bs	Božićno ostrvo	1					
7086104	2078138	ee	Kristmas ƒudomekpo nutome	1					
7086105	2078138	fo	Jólaoyggjin	1					
7086106	2078138	gu	ક્રિસમસ આઇલેન્ડ	1					
7086107	2078138	is	Jólaey	1					
7086108	2078138	kl	Jul-qeqertaq	1					
7086109	2078138	kn	ಕ್ರಿಸ್ಮಸ್ ದ್ವೀಪ	1					
7086110	2078138	mr	ख्रिसमस बेट	1					
7086111	2078138	my	ခရစ်စမတ် ကျွန်း	1					
7086112	2078138	ne	क्रिष्टमस टापु	1					
7086113	2078138	or	ଖ୍ରୀଷ୍ଟମାସ ଦ୍ୱୀପ	1					
7086114	2078138	rm	Insla da Nadal	1					
7086115	2078138	sw	Kisiwa cha Krismasi	1					
7086116	2078138	ta	கிறிஸ்துமஸ் தீவு	1					
7086117	2078138	te	క్రిస్మస్ దీవి	1					
7086118	2078138	ti	ደሴት ክሪስማስ	1					
7086119	2078138	to	Motu Kilisimasi	1					
7086120	2078138	zu	i-Christmas Island	1					
8046928	2078138		Christmas Island						
13738434	2078138	unlc	CXXCH						
16925706	2078138	zh-Hant	聖誕島	1					
16927718	2078138	as	খ্ৰীষ্টমাছ দ্বীপ	1					
16927719	2078138	ce	ГӀайре ӏиса пайхӏамар вина де	1					
16927720	2078138	dz	ཁི་རིསྟ་མེས་མཚོ་གླིང	1					
16927721	2078138	fy	Krysteilan	1					
16927722	2078138	gd	Eilean na Nollaig	1					
16927723	2078138	ha	Tsibirin Kirsmati	1					
16927724	2078138	hy	Սուրբ Ծննդյան կղզի	1					
16927725	2078138	ig	Agwaetiti Christmas	1					
16927726	2078138	jv	Pulo Natal	1					
16927727	2078138	kk	Рождество аралы	1					
16927728	2078138	km	កោះ​គ្រីស្មាស	1					
16927729	2078138	ks	کرِسمَس جٔزیٖرٕ	1					
16927730	2078138	ky	Рождество аралы	1					
16927731	2078138	lb	Chrëschtdagsinsel	1					
16927732	2078138	ln	Esanga ya Mbótama	1					
16927733	2078138	lo	ເກາະຄຣິສມາດ	1					
16927734	2078138	mn	Зул сарын арал	1					
16927735	2078138	no	Christmasøya	1					
16927736	2078138	pa	ਕ੍ਰਿਸਮਿਸ ਟਾਪੂ	1					
16927737	2078138	ps	د کريسمس ټاپو	1					
16927738	2078138	qu	Isla Christmas	1					
16927739	2078138	sd	ڪرسمس ٻيٽ	1					
16927740	2078138	se	Juovllat-sullot	1					
16927741	2078138	si	ක්‍රිස්මස් දූපත	1					
16927742	2078138	so	Jasiiradda Kirismas	1					
16927743	2078138	sq	Ishulli i Krishtlindjes	1					
16927744	2078138	tg	Ҷазираи Крисмас	1					
16927745	2078138	tk	Roždestwo adasy	1					
16927746	2078138	tt	Раштуа утравы	1					
16927747	2078138	ug	مىلاد ئارىلى	1					
16927748	2078138	uz	Rojdestvo oroli	1					
16927749	2078138	wo	Dunu Kirismas	1					
16927750	2078138	yo	Erékùsù Christmas	1					
3057766	2078139	link	https://ru.wikipedia.org/wiki/%D0%9E%D1%81%D1%82%D1%80%D0%BE%D0%B2_%D0%A0%D0%BE%D0%B6%D0%B4%D0%B5%D1%81%D1%82%D0%B2%D0%B0_%28%D0%90%D0%B2%D1%81%D1%82%D1%80%D0%B0%D0%BB%D0%B8%D1%8F%29						
4474262	2078139		Christmas Island						
8114610	2078139	iata	XCH						
15569391	2078141	wkdt	Q23017437						
15983240	2078141		Bean Hill						
15614510	2078142	wkdt	Q23017436						
15983241	2078142		Andrews Point						
15899822	2078145	wkdt	Q23017433						
15983242	2078145		Aldrich Hill						
1888063	6301340	icao	YPXM						
1890378	6301340	iata	XCH						
5806426	6301340	link	https://en.wikipedia.org/wiki/Christmas_Island_Airport						
13384973	6301340	en	Christmas Island Airport	1					
13384974	6301340	en	Christmas Island International Airport						
13384975	6301340	sv	Julöns flygplats	1					
15308214	6301340	wkdt	Q1035011						
7177741	6942279	link	https://en.wikipedia.org/wiki/Christmas_Island_National_Park						
7287311	8173535	en	Christmas Island District High School						
7398999	8216862	en	Christmas Island Golf Club						
8690234	8529528	link	https://en.wikipedia.org/wiki/Silver_City%2C_Christmas_Island						
8748750	8529528		Silver City						
13359477	11694912	link	https://en.wikipedia.org/wiki/Poon_Saan						
15540941	11694912	wkdt	Q7228678						
