17244300	2411431	en	Witham’s Cemetery						
17244301	2411431	en	Withams Cemetery						
1133410	2411432		Meseta del Molino de Viento						
4330033	2411432		Windmill Hill Flats						
17244302	2411433	en	Williams Way Tunnel						
1133411	2411434		Roca Blanca						
4330034	2411434		White Rock						
17244303	2411435	en	Western Beach						
1133412	2411436		Muelle del Carbón						
4330035	2411436		Western Arm						
8678917	2411437	link	https://en.wikipedia.org/wiki/Wellington_Front						
8802914	2411437		Wellington Front						
17244304	2411438	en	Waterport Wharf						
13774054	2411439	unlc	GIWTP						
1133413	2411440		Victoria Stadium						
4330036	2411440		Victoria Sports Centre						
17244305	2411442	en	Upper Galleries						
17244306	2411442	en	Upper Galleries Entrance						
17244307	2411443	en	Trafalgar House						
17244308	2411443	en	Trafalgar Palace Hotel						
5900439	2411444	link	https://en.wikipedia.org/wiki/Trafalgar_Cemetery						
6129016	2411444		Trafalgar Cemetery						
15496699	2411444	wkdt	Q1009226						
17244309	2411445	en	Tower of Homage						
17244310	2411445	en	Moorish Castle						
1133414	2411446		Dockyard Tower						
4330037	2411446		The Tower						
8688054	2411447	link	https://en.wikipedia.org/wiki/Tovey_Battery						
8802915	2411447		Tovey Battery						
17244311	2411449	en	Sunnyside Steps						
17244312	2411450	en	Spyglass Battery						
8070042	2411451	link	https://en.wikipedia.org/wiki/Spur_Battery						
8108847	2411451		Spur Battery						
8075007	2411452	link	https://en.wikipedia.org/wiki/Southport_Gates						
1133415	2411453		Muelle Sur						
4330038	2411453		South Mole						
1133416	2411454		Jumpers Bastion						
1133417	2411454		Baluarte Jumper del Sud						
4330039	2411454		South Jumpers Bastion						
17244313	2411455	en	South Barracks House						
17244314	2411455	en	South Barracks						
17244315	2411456	en	Signal Hill						
1133418	2411457		Cueva Shirley						
4330040	2411457		Shirley Cove						
17244316	2411458	en	Servants’ Block						
17244317	2411459	en	Schomberg Building						
17244318	2411459	en	Schomberg						
17244319	2411460	en	Sandy Bay North						
1133419	2411461		Salto Carrobo						
1133420	2411461		Salto Garrobo						
4330041	2411461		Sandy Bay						
7153979	2411461	link	https://en.wikipedia.org/wiki/Sandy_Bay%2C_Gibraltar						
17244320	2411463	en	Saint Theresa’s Church						
17244321	2411464	en	Saint Peter’s School						
17244322	2411465	en	Saint Michael’s Gate						
7153136	2411466	link	https://en.wikipedia.org/wiki/St._Michael%27s_Cave						
5897187	2411467	link	https://en.wikipedia.org/wiki/Cathedral_of_St._Mary_the_Crowned						
6129017	2411467		Cathedral of Saint Mary’s the Crowned						
17244323	2411470	en	Saint Christopher’s School						
1133421	2411471		Civil Hospital						
4330042	2411471		Saint Bernard’s Hospital						
7552191	2411471	link	https://en.wikipedia.org/wiki/St_Bernard%27s_Hospital						
17244324	2411473	en	Saint Abb’s Head						
17244325	2411475	en	Saccone and Speed Bottling Factory						
17244326	2411476	en	Hillsides Dementia Facility						
17244327	2411476	en	Royal Naval Hospital						
17244328	2411477	en	Royal Gibraltar Yacht Club						
1133422	2411478		Rock Gun						
4330043	2411478		Royal Battery						
1133423	2411480		Muelle Rosia						
4330044	2411480		Rosia Mole						
1133424	2411481		Cala Rosia						
4330045	2411481		Rosia Bay						
8083973	2411481	link	https://en.wikipedia.org/wiki/Rosia_Bay						
8686875	2411483	link	https://en.wikipedia.org/wiki/Rooke_Battery						
8802916	2411483		Rooke Battery						
1133425	2411484		Hotel Peñon						
4330046	2411484		Rock Hotel						
17244329	2411485	en	Rock Battery						
17244330	2411487	en	Ragged Staff Landing						
8672167	2411488	link	https://en.wikipedia.org/wiki/Ragged_Staff_Gates						
15371131	2411488	wkdt	Q15271263						
17244331	2411490	en	Queen’s Gate						
1133426	2411491		Dique Número 2						
1133427	2411491		Dock Number 2						
4330047	2411491		Queen Alexandra Dock Number Two						
17244332	2411492	en	Prince William’s Battery						
8098330	2411493	link	https://en.wikipedia.org/wiki/Princess_Royal%27s_Battery						
8108848	2411493		Princess Royal’s Battery						
8071430	2411494	link	https://en.wikipedia.org/wiki/Princess_Caroline%27s_Battery						
8108849	2411494		Princess Caroline’s Battery						
8101357	2411495	link	https://en.wikipedia.org/wiki/Princess_Anne%27s_Battery						
8108850	2411495		Princess Anne’s Battery						
8091583	2411496	link	https://en.wikipedia.org/wiki/Princess_Amelia%27s_Battery						
8108851	2411496		Princess Amelia’s Battery						
1133428	2411497		Dique Número 1						
1133429	2411497		Dock Number 1						
4330048	2411497		Prince of Wales Dock Number One						
1133430	2411498		Commercial Mole						
4330049	2411498		Passenger Wharf						
1133431	2411499		Punta del Pasagre						
1133432	2411499		Punta Pasaje						
4330050	2411499		Passage Point						
8095549	2411500	link	https://en.wikipedia.org/wiki/Parson%27s_Lodge_Battery						
1133433	2411501	es	Muelle del Arsenal						
4330051	2411501		Ordnance Wharf						
17244333	2411503	en	Old Brewery Barracks						
1133434	2411504		Torre de O’hara						
4330052	2411504		O’Hara’s Battery						
8082875	2411504	link	https://en.wikipedia.org/wiki/O%27Hara%27s_Battery						
17244334	2411505	en	Nun’s Well						
16538114	2411506		Number Two Jetty						
16538115	2411506		Number Two						
16538116	2411507		Number Three Jetty						
16538117	2411507		Number Three						
16538118	2411508		Number One Jetty						
16538119	2411508		Number One						
16538120	2411509		Number Four Jetty						
16538121	2411509		Number Four						
1133435	2411510		Graving Dock						
1133436	2411510		Dique Número 4						
4330053	2411510		Dock Number Four						
16538122	2411511		Number Five Jetty						
16538123	2411511		Number Five						
1133437	2411512		Muelle Norte						
4330054	2411512		North Mole						
1133438	2411513		Baluarte Jumper del Norte						
4330055	2411513		North Jumpers Bastion						
17244335	2411514	en	North Gorge						
17244336	2411515	en	Four Corners						
17244337	2411515	en	North Front						
17244338	2411516	en	New Camp						
17244339	2411517	en	Napier Battery						
17244340	2411518	en	Mount Garden						
17244341	2411519	en	Mount Barbary						
17244342	2411520	en	Mount Alvernia						
1133440	2411521		El Monte						
4330056	2411521		The Mount						
8093062	2411521	link	https://en.wikipedia.org/wiki/The_Mount_%28Gibraltar%29						
17244343	2411523	en	Montarik Building						
17244344	2411523	en	Montarik Hotel						
17244345	2411524	en	Montagu Pavilion						
17244346	2411524	en	Montagu Sea Bathing Pavilion						
1133441	2411526		Alameda de los Monos						
4330057	2411526		Monkeys Alameda						
17244347	2411527	en	Middle Hill Battery						
1133442	2411528		Cerro de Enmedio						
4330058	2411528		Middle Hill						
11293212	2411528	link	https://en.wikipedia.org/wiki/Middle_Hill_%28Gibraltar%29						
17244348	2411529	en	Middle Gate						
1133443	2411530		Peldaños del Mediterraneo						
4330059	2411530		Mediterranean Steps						
8683621	2411530	link	https://en.wikipedia.org/wiki/Mediterranean_Steps						
17244349	2411531	en	Mediterranean Rowing Club						
17244350	2411532	en	Mediterranean Hotel						
17244351	2411533	en	Martin’s Cave						
17244352	2411534	en	Marina Court						
1133444	2411535		Muelle Principal						
4330060	2411535		Main Wharf						
1133445	2411536		Banco Mackerel						
4330061	2411536		Mackerel Bank						
17244353	2411537	en	Lourde’s School						
1133446	2411540	es	Punta Grande de Europa						
7189728	2411540	link	https://en.wikipedia.org/wiki/Europa_Point						
10223949	2411540		Punta Europa						
10223950	2411540	en	Europa Point						
10223951	2411540	en	Great Europa Point						
1133447	2411541	en	Europa Bay						
1133448	2411541		Cala de Europa						
1133449	2411541		Ensenada de Europa						
4330062	2411541		Little Bay						
17244354	2411542	en	Lighthouse Battery						
17244355	2411543	en	The Lido						
1133450	2411544		Cueva de LeVante						
4330063	2411544		Levant Cave						
15480658	2411544	wkdt	Q6534899						
8680387	2411545	link	https://en.wikipedia.org/wiki/Lathbury_Barracks						
8802917	2411545		Lathbury Barracks						
17244356	2411546	en	Laguna Estate						
1133451	2411548		Baluarte del Rey						
8691679	2411548	link	https://en.wikipedia.org/wiki/King%27s_Bastion						
17244357	2411549	en	King George V Memorial Hospital						
1133452	2411550		Dock Number 3						
1133453	2411550		Dique Número 3						
4330064	2411550		King Edward VII Dock Number Three						
17244358	2411551	en	Keightley Way Tunnel						
1133454	2411552		Commercial Square						
4330065	2411552		John Mackintosh Square						
7166180	2411552	link	https://en.wikipedia.org/wiki/John_Mackintosh_Square						
11615918	2411552	es	Plaza de John Mackintosh						
11615919	2411552	fi	John Mackintoshin aukio						
11615920	2411552	hi	जॉन मैकिन्टौश स्क्वयर						
11615921	2411552	ja	ジョン・マッキントッシュ・スクエア						
11615922	2411552	pa	ਜਾਨ ਮੈਕਿੰਟੌਸ਼ ਸਕਵੈਰ						
11615923	2411552	zh	约翰·麦金托什广场						
15316217	2411552	wkdt	Q41321						
8072271	2411553	link	https://en.wikipedia.org/wiki/John_Mackintosh_Hall						
17244359	2411554	en	Jarvis House						
17244360	2411555	en	Ince’s Farm						
1133455	2411556		Exchange Building						
4330066	2411556		House of Assembly						
2944285	2411558	link	https://en.wikipedia.org/wiki/Cathedral_of_the_Holy_Trinity%2C_Gibraltar						
4330067	2411558		Cathedral of the Holy Trinity						
15887878	2411558	wkdt	Q93896						
17244361	2411559	en	The Holy Land						
8677389	2411561	link	https://en.wikipedia.org/wiki/Hayne%27s_Cave_Battery						
8802918	2411561		Hayne’s Cave Battery						
17244362	2411562	en	Hayne’s Cave						
17244363	2411563	en	Halfway Battery						
1133456	2411564		Muelle de Artilleria						
4330068	2411564		Gun Wharf						
1133457	2411565		Punta Guilds						
4330069	2411565		Guild’s Point						
8071980	2411566	link	https://en.wikipedia.org/wiki/Green%27s_Lodge_Battery						
8108852	2411566		Green’s Lodge Battery						
1133458	2411567		Europa Point						
1133459	2411567		Punta Grande de Europa						
4330070	2411567		Great Europa Point						
17244364	2411568	en	Grand Parade						
1133460	2411569		Plaza de las Casamatas						
1133461	2411569		Grand Casemate Square						
4330071	2411569		Grand Casemates Square						
8099110	2411569	link	https://en.wikipedia.org/wiki/Grand_Casemates_Square						
15596088	2411569	wkdt	Q327587						
8075520	2411570	link	https://en.wikipedia.org/wiki/Grand_Casemates_Gates						
8108853	2411570		Grand Casemates Gates						
8081821	2411571	link	https://en.wikipedia.org/wiki/Grand_Casemates						
17244365	2411575	en	Governor’s Look-out						
17244366	2411575	en	Governor’s Look-out Battery						
17244367	2411578	en	The Convent Garden						
17244368	2411578	en	Government House Garden						
1133462	2411579		The Convent						
4330072	2411579		Government House						
7552570	2411579	link	https://en.wikipedia.org/wiki/The_Convent_%28Gibraltar%29						
1133463	2411580		Corhams Cave						
4330073	2411580		Gorham’s Cave						
5898075	2411580	link	https://en.wikipedia.org/wiki/Gorham%27s_Cave						
8089734	2411582	link	https://en.wikipedia.org/wiki/Gibraltar_Harbour						
8108854	2411582		Gibraltar Harbour						
11463363	2411582	fi	Gibraltarin satama						
11463364	2411582	ja	ジブラルタル港						
16471314	2411582	ar	ميناء جبل طارق						
1133464	2411583		Peñon de Gibraltar						
1133465	2411583	la	Mons Calpe						
1133466	2411583	en	The Rock						
1133467	2411583		Gibraltar						
1565418	2411583	es	Peñón de Gibraltar						
1565419	2411583	es	El Peñón						
1565420	2411583	es	La Roca						
1627138	2411583	ca	Gibraltar						
1627139	2411583	ca	Penó de Gibraltar						
2944286	2411583	link	https://en.wikipedia.org/wiki/Rock_of_Gibraltar						
4330074	2411583		Rock of Gibraltar						
8300685	2411583	de	Fels von Gibraltar						
8300686	2411583	fr	Rocher de Gibraltar						
8300687	2411583	it	Rocca di Gibilterra						
8300688	2411583	nl	Rots van Gibraltar						
8300689	2411583	fi	Gibraltarinvuori						
8300690	2411583	war	Bato han Gibraltar						
8300691	2411583	ar	صخرة جبل طارق	1					
8300692	2411583	uk	Гібралтарська скеля						
8300693	2411583	ca	Penyal de Gibraltar						
8300694	2411583	id	Batu Gibraltar						
8300695	2411583	gl	Rocha de Xibraltar						
8300696	2411583	da	Gibraltarklippen						
8300697	2411583	vi	Núi Gibraltar						
8300698	2411583	oc	Arròc de Gibartar						
8300699	2411583	he	צוק גיברלטר						
8300700	2411583	ja	ジブラルタルの岩						
8300701	2411583	be	Гібралтарская скала						
8300702	2411583	is	Gíbraltarhöfði						
8300703	2411583	ne	गिब्राल्टारको चट्टान						
8300704	2411583	pl	Skała Gibraltarska						
8300705	2411583	pt	Rochedo de Gibraltar						
8300706	2411583	ru	Гибралтарская скала						
8300707	2411583	zh	直布羅陀巨巖						
13928500	2411583	it	Roccia di Gibilterra	1					
16471312	2411583	link	https://ar.wikipedia.org/wiki/%D8%B5%D8%AE%D8%B1%D8%A9_%D8%AC%D8%A8%D9%84_%D8%B7%D8%A7%D8%B1%D9%82						
16471313	2411583	wkdt	Q690347						
1133468	2411584		Bay of Algeciras						
1133469	2411584		Bahía de Algeciras						
1133470	2411584		Gibraltar Bay						
1627140	2411584	ca	Badia de Gibraltar						
2944287	2411584	link	https://en.wikipedia.org/wiki/Bay_of_Gibraltar						
1133471	2411585		Gibraltar						
1565778	2411585	eo	Ĝibraltaro						
1596068	2411585	de	Gibraltar						
1596069	2411585	en	Gibraltar						
1596070	2411585	es	Gibraltar	1					
1596071	2411585	ar	محمية جبل طارق						
1596072	2411585	ast	Xibraltar						
1596073	2411585	bg	Гибралтар						
1596074	2411585	bs	Gibraltar						
1596075	2411585	ca	Gibraltar						
1596076	2411585	cs	Gibraltar						
1596077	2411585	da	Gibraltar						
1596078	2411585	el	Γιβραλτάρ						
1596079	2411585	et	Gibraltar						
1596080	2411585	eu	Gibraltar						
1596081	2411585	fa	جبل‌الطارق						
1596082	2411585	fi	Gibraltar						
1596083	2411585	fr	Gibraltar						
1596084	2411585	gl	Xibraltar - Gibraltar						
1596085	2411585	he	גיברלטר						
1596086	2411585	hr	Gibraltar						
1596087	2411585	hu	Gibraltár						
1596088	2411585	ia	Gibraltar						
1596089	2411585	id	Gibraltar						
1596090	2411585	io	Gibraltar						
1596091	2411585	is	Gíbraltar						
1596092	2411585	it	Gibilterra						
1596093	2411585	ja	ジブラルタル						
1596094	2411585	ko	지브롤터						
1596095	2411585	la	Calpe						
1596096	2411585	lb	Gibraltar						
1596097	2411585	lt	Gibraltaras						
1596098	2411585	mi	Kamaka						
1596099	2411585	nds	Gibraltar						
1596100	2411585	nl	Gibraltar						
1596101	2411585	nn	Gibraltar						
1596102	2411585	no	Gibraltar						
1596103	2411585	pl	Gibraltar						
1596104	2411585	pt	Gibraltar						
1596105	2411585	ro	Gibraltar						
1596106	2411585	ru	Гибралтар						
1596107	2411585	scn	Gibbilterra						
1596108	2411585	sk	Gibraltár						
1596109	2411585	sl	Gibraltar						
1596110	2411585	sr	Гибралтар						
1596111	2411585	sv	Gibraltar						
1596112	2411585	tl	Gibraltar						
1596113	2411585	tr	Cebelitarık						
1596114	2411585	uk	Ґібралтар						
1596115	2411585	wa	Djibraltar						
1596116	2411585	zh	直布罗陀						
5423935	2411585	link	https://en.wikipedia.org/wiki/Gibraltar						
7483675	2411585	iata	GIB						
13774053	2411585	unlc	GIGIB						
16471306	2411585	link	https://ar.wikipedia.org/wiki/%D8%AC%D8%A8%D9%84_%D8%B7%D8%A7%D8%B1%D9%82						
16471307	2411585	ar	جبل طارق	1					
1894809	2411586	de	Gibraltar	1					
1894810	2411586	es	Gibraltar	1					
1894811	2411586	en	Gibraltar	1					
1894812	2411586	fr	Gibraltar	1					
1894813	2411586	pl	Gibraltar	1					
1894814	2411586	ar	مستعمرة جبل طارق						
1894815	2411586	ast	Xibraltar						
1894816	2411586	br	Jibraltar	1					
1894817	2411586	bs	Gibraltar	1					
1894818	2411586	ca	Gibraltar	1					
1894819	2411586	cs	Gibraltar	1					
1894820	2411586	da	Gibraltar	1					
1894821	2411586	el	Γιβραλτάρ	1					
1894822	2411586	eo	Ĝibraltaro	1					
1894823	2411586	et	Gibraltar	1					
1894824	2411586	eu	Gibraltar	1					
1894825	2411586	fa	جبل طارق						
1894826	2411586	fi	Gibraltar	1					
1894827	2411586	gl	Xibraltar - Gibraltar						
1894828	2411586	he	גיברלטר	1					
1894829	2411586	hr	Gibraltar	1					
1894830	2411586	hu	Gibraltár	1					
1894831	2411586	ia	Gibraltar	1					
1894832	2411586	id	Gibraltar	1					
1894833	2411586	io	Gibraltar						
1894834	2411586	is	Gíbraltar	1					
1894835	2411586	it	Gibilterra	1					
1894836	2411586	ja	ジブラルタル	1					
1894837	2411586	ka	გიბრალტარი	1					
1894838	2411586	ko	지브롤터	1					
1894839	2411586	kw	Jibraltar						
1894840	2411586	la	Calpe						
1894841	2411586	lb	Gibraltar	1					
1894842	2411586	lt	Gibraltaras	1					
1894843	2411586	mi	Kāmaka						
1894844	2411586	nds	Gibraltar						
1894845	2411586	nl	Gibraltar	1					
1894846	2411586	nn	Gibraltar						
1894847	2411586	no	Gibraltar	1					
1894848	2411586	oc	Gibraltar						
1894849	2411586	pt	Gibraltar	1					
1894850	2411586	ro	Gibraltar	1					
1894851	2411586	ru	Гибралтар	1					
1894852	2411586	scn	Gibbilterra						
1894853	2411586	hbs	Gibraltar						
1894854	2411586	sk	Gibraltár	1					
1894855	2411586	sl	Gibraltar	1					
1894856	2411586	sq	Gjibraltari						
1894857	2411586	sr	Гибралтар	1					
1894858	2411586	sv	Gibraltar	1					
1894859	2411586	tl	Hibraltar						
1894860	2411586	tr	Cebelitarık	1					
1894861	2411586	uk	Ґібралтар						
1894862	2411586	wa	Djibraltar						
1894863	2411586	zh-CN	直布罗陀						
1980545	2411586	hsb	Gibraltar						
1980546	2411586	lv	Gibraltārs	1					
1980547	2411586	oc	Gibartar						
1980548	2411586	pam	Gibraltar						
1980549	2411586	th	ยิบรอลตาร์	1					
2418426	2411586	am	ጂብራልተር	1					
2418427	2411586	ar	جبل طارق	1					
2418428	2411586	be	Гібралтар	1					
2418429	2411586	bg	Гибралтар	1					
2418430	2411586	bn	জিব্রাল্টার	1					
2418431	2411586	bo	ཇིབ་རཱལ་ཊར།	1					
2418432	2411586	cy	Gibraltar	1					
2418433	2411586	fa	جبل‌الطارق	1					
2418434	2411586	ga	Giobráltar	1					
2418435	2411586	hi	जिब्राल्टर	1					
2418436	2411586	ka	ჰიბრალტარი	1					
2418437	2411586	mk	Гибралтар	1					
2418438	2411586	ml	ജിബ്രാൾട്ടർ	1					
2418439	2411586	ms	Gibraltar	1					
2418440	2411586	mt	Ġibiltà	1					
2418441	2411586	nb	Gibraltar	1					
2418442	2411586	se	Gibraltar	1					
2919875	2411586	link	https://en.wikipedia.org/wiki/Gibraltar						
3051370	2411586	link	https://ru.wikipedia.org/wiki/%D0%93%D0%B8%D0%B1%D1%80%D0%B0%D0%BB%D1%82%D0%B0%D1%80						
4330075	2411586		Gibraltar						
7086886	2411586	af	Gibraltar	1					
7086887	2411586	ak	Gyebralta	1					
7086888	2411586	az	Cəbəllütariq	1					
7086889	2411586	bm	Zibralitari	1					
7086890	2411586	ee	Gibraltar nutome	1					
7086891	2411586	ff	Jibraltaar	1					
7086892	2411586	fo	Gibraltar	1					
7086893	2411586	gl	Xibraltar	1					
7086894	2411586	gu	જીબ્રાલ્ટર	1					
7086895	2411586	ha	Jibaraltar	1					
7086896	2411586	ki	Jibralta	1					
7086897	2411586	kl	Gibraltar	1					
7086898	2411586	kn	ಗಿಬ್ರಾಲ್ಟರ್	1					
7086899	2411586	lg	Giburalita	1					
7086900	2411586	ln	Zibatalɛ	1					
7086901	2411586	lu	Jibeletale	1					
7086902	2411586	mg	Zibraltara	1					
7086903	2411586	mr	जिब्राल्टर	1					
7086904	2411586	my	ဂျီဘရော်လ်တာ	1					
7086905	2411586	nd	Gibraltar	1					
7086906	2411586	ne	जिब्राल्टार	1					
7086907	2411586	or	ଜିବ୍ରାଲ୍ଟର୍	1					
7086908	2411586	rm	Gibraltar	1					
7086909	2411586	rn	Juburalitari	1					
7086910	2411586	sg	Zibraltära, Zibaratära	1					
7086911	2411586	si	ජිබ්‍රෝල්ටාව	1					
7086912	2411586	sn	Gibraltar	1					
7086913	2411586	so	Gibraltar	1					
7086914	2411586	sw	Gibraltar	1					
7086915	2411586	ta	ஜிப்ரால்டர்	1					
7086916	2411586	te	జిబ్రాల్టర్	1					
7086917	2411586	ti	ጊብራልታር	1					
7086918	2411586	to	Sipalālitā	1					
7086919	2411586	uk	Гібралтар	1					
7086920	2411586	ur	جبل الطارق	1					
7086921	2411586	vi	Gibraltar	1					
7086922	2411586	yo	Gibaratara	1					
7086923	2411586	zu	i-Gibraltar	1					
7580123	2411586	gv	Gibraaltar						
7580124	2411586	rw	Giburalitari						
7580125	2411586	bpy	জিব্রালটার						
7580126	2411586	vec	Gibiltera						
7580127	2411586	gd	Giobraltair						
7580128	2411586	ur	جبرالٹر						
7580129	2411586	arz	جيبرالتار						
7580130	2411586	ang	Calpis						
7580131	2411586	wo	Jibraltaar						
7580132	2411586	tt	Гибралтар	1					
7580133	2411586	pnb	جبرالٹر						
7580134	2411586	kk	Гибралтар	1					
7580135	2411586	yi	גיבראלטאר						
7580136	2411586	pms	Gibiltèra						
7580137	2411586	os	Гибралтар						
7580138	2411586	pih	Jibrulta						
7580139	2411586	wuu	直布罗陀						
7580140	2411586	hy	Ջիբրալթար	1					
7580141	2411586	lij	Gibiltæra						
7580142	2411586	lad	Jibraltar						
7580143	2411586	nrm	Dgibrâltar						
7580144	2411586	so	Jibraltaar						
7580145	2411586	dv	ޖަބަލްޠާރިޤު						
7580146	2411586	xal	Гибралта Балһсн						
7580147	2411586	an	Chibraltar						
7580148	2411586	cv	Гибралтар						
7580149	2411586	az	Cəbəli-Tariq						
7580150	2411586	lmo	Gibiltera						
7580151	2411586	tg	Гибралтар	1					
7580152	2411586	ta	ஜிப்ரால்ட்டர்						
9861312	2411586	zh-TW	直布羅陀						
11085442	2411586	zh	直布羅陀						
16471308	2411586	link	https://ar.wikipedia.org/wiki/%D8%AC%D8%A8%D9%84_%D8%B7%D8%A7%D8%B1%D9%82						
16925735	2411586	zh-Hant	直布羅陀	1					
16928358	2411586	as	জিব্ৰাল্টৰ	1					
16928359	2411586	ce	Гибралтар	1					
16928360	2411586	dz	ཇིབ་རཱལ་ཊར	1					
16928361	2411586	fy	Gibraltar	1					
16928362	2411586	gd	Diobraltar	1					
16928363	2411586	ig	Gibraltar	1					
16928364	2411586	jv	Gibraltar	1					
16928365	2411586	km	ហ្ស៊ីប្រាល់តា	1					
16928366	2411586	ks	جِبرالٹَر	1					
16928367	2411586	ku	Cîbraltar	1					
16928368	2411586	ky	Гибралтар	1					
16928369	2411586	lo	ຈິບບຣອນທາ	1					
16928370	2411586	mn	Гибралтар	1					
16928371	2411586	pa	ਜਿਬਰਾਲਟਰ	1					
16928372	2411586	ps	جبل الطارق	1					
16928373	2411586	qu	Gibraltar	1					
16928374	2411586	sd	جبرالٽر	1					
16928375	2411586	sq	Gjibraltar	1					
16928376	2411586	tk	Gibraltar	1					
16928377	2411586	ug	جەبىلتارىق	1					
16928378	2411586	uz	Gibraltar	1					
16928379	2411586	wo	Sibraltaar	1					
16928380	2411586	yi	גיבראַלטאַר	1					
16928381	2411586	zh	直布罗陀	1					
17244369	2411587	en	Genoese Battery						
17244370	2411588	en	Frontier Gate						
17244371	2411589	en	Flagstaff Steps						
17244372	2411590	en	Europa Point Cricket Pitch						
17244373	2411590	en	Europa Sports Ground						
17244374	2411590	en	Europa Point Sports Complex						
1133472	2411591		Punta Europa						
4330076	2411591	en	Little Europa Point						
10223952	2411591	es	Punta Chica de Europa						
15397766	2411591	wkdt	Q24698064						
1133473	2411592		Paso de Europa						
4330077	2411592		Europa Pass						
1133474	2411593		Meseta de Europa						
4330078	2411593		Europa Flats						
8683945	2411595	link	https://en.wikipedia.org/wiki/Engineer_Battery						
8802919	2411595		Engineer Battery						
17244375	2411597	en	Edinburgh Estate						
17244376	2411597	en	Edinburgh House						
1133475	2411598		Playa Oriental						
4330079	2411598		Eastern Beach						
1133476	2411599		Dudley Ward Way Tunnel						
4330080	2411599		Dudley Ward Tunnel						
7554205	2411599	link	https://en.wikipedia.org/wiki/Dudley_Ward_Tunnel						
17244377	2411601	en	South Mole Main Gate						
17244378	2411601	en	Dockyard South Gate						
11298027	2411603	link	https://en.wikipedia.org/wiki/Devil%27s_Tower_Camp						
11392573	2411603		Devil’s Tower Camp						
8676698	2411604	link	https://en.wikipedia.org/wiki/Devil%27s_Tongue_Battery						
8081262	2411605	link	https://en.wikipedia.org/wiki/Devil%27s_Gap_Battery						
8108855	2411605		Devil’s Gap Battery						
1133477	2411606		Muelle Rompeolas						
4330081	2411606		Detached Mole						
11295937	2411607	link	https://en.wikipedia.org/wiki/Defensible_Barracks						
11392574	2411607		Defensible Barracks						
17244379	2411608	en	Dead Man’s Beach						
17244380	2411608	en	Deadmans Beach						
17244381	2411609	en	Cross of Sacrifice						
1133478	2411611	es	Cormorant Dársena						
1133479	2411611		Cormorant Reclamation						
4330082	2411611		Cormorant Camber						
17244382	2411611	es	Dársena de Botes						
17244383	2411611	en	Boat Camber						
17244384	2411611	en	Auxiliary Camber						
8092816	2411612	link	https://en.wikipedia.org/wiki/Coaling_Island						
17244385	2411614	en	City Council Pumping Station						
17244386	2411615	en	Chilton Court						
13721188	2411616	link	https://en.wikipedia.org/wiki/Flat_Bastion						
13721189	2411616	en	St. Jago's Bastion				1		
13721190	2411616	es	Baluarte de Santiago				1		
16766478	2411616	en	Saint Jago's Bastion						
1133480	2411619		Catalan						
1133481	2411619		Caleta						
1133482	2411619		Catalan Bay Village						
7197134	2411619	link	https://en.wikipedia.org/wiki/Catalan_Bay						
15855893	2411619	wkdt	Q182528						
16691793	2411620	link	https://en.wikipedia.org/wiki/Catalan_Bay						
16694041	2411620		Catalan Bay						
1133483	2411621		Cala Camp						
4330083	2411621		Camp Bay						
17244387	2411622	en	Calpe Rowing Club						
17244388	2411623	en	The Caleta Hotel						
17244389	2411623	en	Caleta Palace Hotel						
17244390	2411624	en	Caledonian Canal						
15438736	2411625	wkdt	Q24698172						
15969129	2411625		Buffadero Battery Quarry						
8678052	2411626	link	https://en.wikipedia.org/wiki/Buffadero_Battery						
8802920	2411626		Buffadero Battery						
17244391	2411627	en	Buena Vista Cottage						
17244392	2411628	en	Buena Vista						
1133484	2411629		Buena Vista Married Quarters						
4330084	2411629		Buena Vista						
17244393	2411629	en	Buena Vista Mews						
17244394	2411630	en	Bruce’s Farm						
8096026	2411631	link	https://en.wikipedia.org/wiki/Bristol_Hotel%2C_Gibraltar						
8108856	2411631		Bristol Hotel						
8079502	2411632	link	https://en.wikipedia.org/wiki/Breakneck_Battery						
8108857	2411632		Breakneck Battery						
17244395	2411633	en	Both Worlds						
17244396	2411633	en	Both Worlds Chalets						
17244397	2411635	en	Bleak House						
15552131	2411636	wkdt	Q24698054						
15969130	2411636		Bleak Beach						
17244398	2411637	en	Bland’s Foundry						
1133486	2411638		Cueva Blackstrag						
4330086	2411638		Blackstrap Cove						
1133487	2411639		Mala Bahía						
17244399	2411639		Blackstrap Bay						
17244400	2411641	en	Beavers Promenade						
17244401	2411642	en	Marina Bay						
17244402	2411642	en	Bayside Marina						
1133488	2411644		The Palace						
4330088	2411644		Arengo’s Palace						
17244403	2411645	en	Apes Den						
8693287	2411646	link	https://en.wikipedia.org/wiki/Alexandra_Battery						
8802921	2411646		Alexandra Battery						
17244404	2411646	en	South Mole Bastion						
17244405	2411649	en	Ailsa Craig						
17245718	2517233	en	Gibraltar-Spain Neutral Zone						
1893515	6301797	iata	GIB						
1893516	6301797	icao	LXGB						
5762853	6301797	link	https://en.wikipedia.org/wiki/Gibraltar_International_Airport						
5892105	6301797	en	Gibraltar International Airport	1					
5893121	6301797	de	Flughafen Gibraltar						
5893122	6301797	es	Aeropuerto de Gibraltar						
5893123	6301797	fr	Aéroport international de Gibraltar						
5893124	6301797	it	Aeroporto di Gibilterra						
5893125	6301797	he	נמל התעופה גיברלטר						
5893126	6301797	zh	直布羅陀機場						
5893127	6301797	no	Gibraltar lufthavn						
5893128	6301797	tr	Cebelitarık Havalimanı						
5893129	6301797	hu	Gibraltári repülőtér						
5893130	6301797	pl	Port lotniczy Gibraltar						
5893131	6301797	ru	Гибралтар						
10840668	6301797	en	North Front Airport						
15380294	6301797	wkdt	Q501083						
16471309	6301797	ar	مطار جبل طارق	1					
16471310	6301797	link	https://ar.wikipedia.org/wiki/%D9%85%D8%B7%D8%A7%D8%B1_%D8%AC%D8%A8%D9%84_%D8%B7%D8%A7%D8%B1%D9%82						
16471311	6301797	ar	مطار جبل طارقالدولي						
7153661	6693878	link	https://en.wikipedia.org/wiki/Moorish_Castle						
15362676	6693878	wkdt	Q249047						
8089987	6693879	link	https://en.wikipedia.org/wiki/Great_Siege_Tunnels						
10223953	9855505	link	https://en.wikipedia.org/wiki/Europa_Point_Lighthouse						
10223954	9855505		Trinity Lighthouse at Europa Point						
10223955	9855505		Victoria Tower						
10223956	9855505		La Farola						
16202514	10280700	link	https://en.wikipedia.org/wiki/Charles_V_Wall						
15544846	10280705	wkdt	Q24433						
16168839	10280705	link	https://en.wikipedia.org/wiki/Gibraltar_Botanic_Gardens						
15722221	10280713	wkdt	Q24698050						
2944284	10280716	link	https://en.wikipedia.org/wiki/Victoria_Stadium_%28Gibraltar%29						
16531115	12129137	en	No. 5 Wharf				1		
17248565	12402446	en	British War Memorial						
17248566	12402447	en	American War Memorial						
17248567	12402448	en	Winston Churchill Avenue						
17248568	12402449	en	Forbes’ Quarry						
17249367	12402450	en	University of Gibraltar						
