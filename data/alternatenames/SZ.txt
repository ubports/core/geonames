518826	934820		Libe						
518827	934820		Zibe River						
4311833	934820		Zibe						
518828	934821		White Imbuluzi						
518829	934821		White Umbuluzi						
4311834	934821		White Umbuluzi River						
518830	934823		Vonde Kop						
4311835	934823		Vondo Kop						
518831	934824		Vlagati Kop						
4311836	934824		Vilakati Kop						
518832	934825		Vigisijula						
518833	934825		Vig Isiyula						
4311837	934825		Vikisijula						
518834	934827		Usutu Mission						
4311838	934827		Usutu						
518835	934828		Umtilane						
518836	934828		Mtilane River						
4311839	934828		Umtilane River						
518837	934829		Umsundu						
4311840	934829		Umshundu						
15412733	934829	wkdt	Q31592632						
518838	934830		Ulambongwenya						
518839	934830		Umlambongwenya						
4311841	934830		Umlambongwenya River						
518840	934831		Umkomozana						
4311842	934831		Umkomozana River						
518841	934833		Umbuyana						
4311843	934833		Umbuyana River						
15236298	934837	wkdt	Q31592694						
15959132	934837		Umbanjane Kop						
518842	934840		Tahaneni						
4311844	934840		Tshaneni						
7072615	934840	link	https://en.wikipedia.org/wiki/Tshaneni						
15289208	934840	wkdt	Q7849602						
518843	934841		Swasieland						
518845	934841		Ngwane						
1555035	934841	am	ሱዋዚላንድ						
1555036	934841	ar	إسواتيني	1					
1555037	934841	bg	Есватини	1					
1555038	934841	ca	eSwatini	1					
1555039	934841	cs	Svazijsko						
1555040	934841	cy	Eswatini	1					
1555041	934841	da	Eswatini	1					
1555042	934841	de	Eswatini	1					
1555043	934841	el	Εσουατίνι	1					
1555044	934841	en	Swaziland				1		
1555045	934841	eo	Svazilando	1					
1555046	934841	es	Esuatini	1					
1555047	934841	et	eSwatini	1					
1555048	934841	eu	Swazilandia	1					
1555049	934841	fa	سوازیلند	1					
1555050	934841	fi	Swazimaa	1					
1555051	934841	fo	Esvatini	1					
1555052	934841	fr	Swaziland	1	1				
1555053	934841	ga	eSuaitíní	1					
1555054	934841	he	סוואזילנד						
1555055	934841	hi	सुआजीलैंड						
1555056	934841	hr	Svazi	1					
1555057	934841	hu	Eswatini	1					
1555058	934841	hy	Էսվատինի	1					
1555059	934841	id	eSwatini	1					
1555060	934841	is	Svasíland	1					
1555061	934841	it	Swaziland	1					
1555062	934841	ja	スワジランド	1					
1555063	934841	ka	სვაზილანდი	1					
1555064	934841	km	ស្វាស៊ីឡង់	1					
1555065	934841	ko	스와질랜드						
1555066	934841	lo	ສະ​ວາ​ຊິ​ແລນ	1					
1555067	934841	lt	Svazilendas						
1555068	934841	lv	Svatini	1					
1555069	934841	mk	Свазиленд	1					
1555070	934841	ms	Swaziland	1					
1555071	934841	mt	is-Swaziland	1					
1555072	934841	nb	Swaziland	1					
1555073	934841	nl	Eswatini	1					
1555074	934841	nn	Swaziland						2018
1555075	934841	pl	Suazi	1					
1555076	934841	pt	Essuatíni	1					
1555077	934841	ro	Eswatini	1					
1555078	934841	ru	Эсватини	1					
1555079	934841	sk	Eswatini	1					
1555080	934841	sl	Svazi	1					
1555081	934841	sq	Esvatini	1					
1555082	934841	sr	Свазиленд	1					
1555083	934841	sv	Eswatini	1					
1555084	934841	th	เอสวาตีนี	1					
1555085	934841	tr	Esvatini	1					
1555086	934841	uk	Есватіні	1					
1555087	934841	vi	Eswatini	1					
1555088	934841	zh	斯威士兰	1					
1618784	934841	es	Swazilandia						
1618785	934841	af	Swaziland						
1618786	934841	am	ስዋዚላንድ	1					
1618787	934841	an	Swazilandia						
1618788	934841	ang	Swasiland						
1618789	934841	ar	سوازيلند						
1618790	934841	bg	Свазиленд						
1618791	934841	bs	Svazi						
1618792	934841	gd	Dùthaich nan Suasaidh						
1618793	934841	gl	Suacilandia - Swaziland						
1618794	934841	he	סווזילנד	1					
1618795	934841	hi	स्वाज़ीलैण्ड						
1618796	934841	io	Swazilando						
1618797	934841	ko	스와질란드	1					
1618798	934841	ku	Swazîlenda	1					
1618799	934841	kw	Pow Swati						
1618800	934841	lt	Svazilandas	1					
1618801	934841	nds	Swasiland						
1618802	934841	no	Swaziland						2018
1618803	934841	oc	Swaziland						
1618804	934841	sa	स्वाजीलैंड						
1618805	934841	hbs	Svazilend						
1618806	934841	sq	Suazilendi						
1618807	934841	ss	Umbuso weSwatini						
1618808	934841	th	ประเทศสวาซิแลนด์						
1618809	934841	tl	Suwasilandya						
1618810	934841	tr	Swaziland						
1618811	934841	uk	Свазиленд						
1618812	934841	vi	Swaziland						
1632762	934841	el	Σουαζιλάνδη						
1632763	934841	la	Swazia						
1632764	934841	sw	Uswazi	1					
1632765	934841	ug	سۋازىلېند						
1894152	934841	br	Swaziland						
1894153	934841	cy	Gwlad Swasi						
1894154	934841	ilo	Suasilandia						
1894155	934841	pam	Swaziland						
1894156	934841	tg	Свазиленд	1					
1977757	934841	ast	Swazilandia						
1977758	934841	frp	Souazilande						
1977759	934841	nov	Swazilande						
1977760	934841	qu	Swasilandya						
1977761	934841	qu	Swasisuyu						
1977762	934841	vo	Svasiyän						
2418353	934841	be	Эсватыні	1					
2418354	934841	bn	ইসওয়াতিনি	1					
2418355	934841	cs	Swazijsko	1					
2418356	934841	fa	سوازیلاند	1					
2418357	934841	gl	Eswatini	1					
2418358	934841	hi	स्वाज़ीलैंड	1					
2418359	934841	hr	Svaziland	1					
2418360	934841	ia	Swazilandia	1					
2418361	934841	ja	スワジランド王国	1					
2418362	934841	se	Svazieana	1					
2418363	934841	ur	سواتنی	1					
2920047	934841	link	https://en.wikipedia.org/wiki/Eswatini						
4311845	934841		Kingdom of Swaziland				1		
7091003	934841	ak	Swaziland	1					
7091004	934841	az	Esvatini	1					
7091005	934841	bm	Swazilandi	1					
7091006	934841	bs	Esvatini	1					
7091007	934841	ee	Swaziland nutome	1					
7091008	934841	ff	Swaasilannda	1					
7091009	934841	gu	એસ્વાટીની	1					
7091010	934841	ha	Eswatini	1					
7091011	934841	ki	Uswazi	1					
7091012	934841	kl	Swazilandi	1					
7091013	934841	kn	ಸ್ವಾತಿನಿ	1					
7091014	934841	ku	سوازیلاند	1					
7091015	934841	lg	Swazirandi	1					
7091016	934841	ln	Swazilandi	1					
7091017	934841	lu	Swazilandi	1					
7091018	934841	mg	Soazilandy	1					
7091019	934841	ml	സ്വാസിലൻഡ്	1					
7091020	934841	mr	स्वाझिलँड	1					
7091021	934841	nd	Swaziland	1					
7091022	934841	ne	स्वाजिल्याण्ड	1					
7091023	934841	or	ଇସ୍ୱାତିନୀ	1					
7091024	934841	rm	Eswatini	1					
7091025	934841	rn	Suwazilandi	1					
7091026	934841	sg	Swäzïlânde	1					
7091027	934841	sn	Swaziland	1					
7091028	934841	so	Eswaatiini	1					
7091029	934841	ta	எஸ்வாட்டீனி	1					
7091030	934841	te	ఈస్వాటిని	1					
7091031	934841	ti	ኢስዋቲኒ	1					
7091032	934841	to	Suasilani	1					
7091033	934841	yo	Saṣiland	1					
7091034	934841	zu	i-Swaziland	1					
8046214	934841		Swaziland				1		
13694806	934841	uk	Королі́вство Есваті́ні						
13694807	934841	uk	Королі́вство Е-Cваті́ні						
13694808	934841	uk	Нгване						
13694809	934841	uk	Нґване						
13694811	934841	be	Эсваціні						
13694812	934841	bg	Кралство Есватини						
13694813	934841	fr	Eswatini	1					
13694814	934841	ro	Regatul eSwatini						
13694815	934841	pt	Reino de eSwatini						
13694955	934841	fr	Royaume du Swaziland						
13701312	934841		Eswatini	1					
13701313	934841		Kingdom of Eswatini						
13858018	934841	en	Eswatini	1				2018	
16339577	934841	nb	Eswatini	1				2018	
16339578	934841	nn	Eswatini	1				2018	
16339579	934841	no	Eswatini	1				2018	
16925865	934841	zh-Hant	史瓦濟蘭	1					
16931375	934841	af	Eswatini	1					
16931376	934841	as	ইচ্চুটিনি	1					
16931377	934841	br	Eswatini	1					
16931378	934841	ce	Свазиленд	1					
16931379	934841	dz	སུ་ཝ་ཛི་ལེནཌ	1					
16931380	934841	fy	Swazilân	1					
16931381	934841	gd	eSwatini	1					
16931382	934841	ig	Eswatini	1					
16931383	934841	jv	Swasiland	1					
16931384	934841	kk	Свазиленд	1					
16931385	934841	ks	سُوزِلینڑ	1					
16931386	934841	ky	Свазиленд	1					
16931387	934841	lb	Swasiland	1					
16931388	934841	mn	Эсватини	1					
16931389	934841	my	ဆွာဇီလန်	1					
16931390	934841	pa	ਇਸਵਾਤੀਨੀ	1					
16931391	934841	ps	اسواټيني	1					
16931392	934841	qu	Suazilandia	1					
16931393	934841	sd	ايسواٽني	1					
16931394	934841	si	එස්වාටිනි	1					
16931395	934841	tk	Eswatini	1					
16931396	934841	tt	Свазиленд	1					
16931397	934841	ug	سىۋېزىلاند	1					
16931398	934841	uz	Svazilend	1					
16931399	934841	wo	Suwasilànd	1					
16931400	934841	yi	סוואַזילאַנד	1					
518846	934843		Stokoto River						
518847	934843		Stokoto						
4311846	934843		Stokodo River						
16949222	934843		Stokodo						
518848	934845		Statweni						
4311847	934845		Stataweni						
518851	934849		Sivuli Hill						
4311849	934849		Sivule Hill						
518852	934850		Sibobela						
4311850	934850		Sitobela						
518853	934851		Stegi						
1615350	934851	es	Siteki						
2929527	934851	link	https://en.wikipedia.org/wiki/Siteki						
4311851	934851		Siteki						
11760605	934851	fr	Siteki						
14988108	934851	wkdt	Q2300140						
518854	934852		Sipofanenibrug						
518855	934852		Sipofaneni Bridge						
518856	934852		Spofanene						
518857	934852		Sipofaneni						
2929528	934852	link	https://en.wikipedia.org/wiki/Siphofaneni						
4311852	934852		Siphofaneni						
518858	934853		Sepanga Kop						
4311853	934853		Sipanga Kop						
15566055	934854	wkdt	Q31592755						
15959133	934854		Sinyamatulu						
518859	934855		Sinceni						
4311854	934855		Singceni						
518860	934856		Simoniet						
4311855	934856		Simoniet River						
518861	934857		Similane Kop						
4311856	934857		Simelane Kop						
518862	934859		Isekupi						
518863	934859		Isikupi						
518864	934859		Secupa Kop						
4311857	934859		Sikupa Kop						
2929529	934862	link	https://en.wikipedia.org/wiki/Sidvokodvo						
13800093	934862	unlc	SZ7DS						
518865	934863		Sidokodo						
4311858	934863		Sidokodo River						
518866	934864		Sigunusa						
2929530	934864	link	https://en.wikipedia.org/wiki/Sicunusa						
4311859	934864		Sicunusa						
15535923	934864	wkdt	Q7507699						
518867	934865		Sibowe						
4311860	934865		Sibowe River						
518868	934867		Hlatikulu District						
2185647	934867		Shiselweni	1	1				
5650092	934867		Shiselweni District						
5700617	934867	link	https://en.wikipedia.org/wiki/Shiselweni_District						
11760602	934867	fr	Shiselweni						
15352681	934867	wkdt	Q845934						
518869	934869		Seyama Kop						
4311861	934869		Sayama Kop						
2929531	934870	link	https://en.wikipedia.org/wiki/Sandlane						
4311862	934870		Sandlane						
15365091	934870	wkdt	Q7416397						
518870	934871		Salugase						
4311863	934871		Salugase River						
518871	934879		Poponyana River						
518872	934879		Poponyana						
4311864	934879		Poponyane						
1615375	934881	es	Piggs Peak						
2929532	934881	link	https://en.wikipedia.org/wiki/Piggs_Peak						
4311865	934881		Piggs Peak						
14975591	934881	wkdt	Q1950487						
518873	934882		The Peak						
4311866	934882		Piggs Peak						
518874	934884		Nyonani Kop						
4311867	934884		Nyonyane Kop						
15141221	934884	wkdt	Q31592889						
518875	934886		Nyonyana						
4311868	934886		Nyonyana River						
518876	934887		Nyetane						
518877	934887		Inyetyan						
4311869	934887		Nyetane River						
518878	934891		Ntungulu Kop						
4311870	934891		Ntungula Kop						
518879	934892		Mtotosha Kop						
4311871	934892		Ntototsha Kop						
518880	934894		Ntandes						
518881	934894		Ntondosi						
4311872	934894		Ntondozi						
518882	934895		Intabinezimpise						
518883	934895		Ntabinezimpise						
4311873	934895		Ntabinezimpisi						
518884	934898		Ntababomvu						
4311874	934898		Ntababomvu Hills						
518885	934899		Nsongweni						
4311875	934899		Nsongweni River						
518886	934900		Nsoka						
2929533	934900	link	https://en.wikipedia.org/wiki/Nsoko						
4311876	934900		Nsoko						
15277347	934900	wkdt	Q7067578						
518887	934902		Nsagana						
4311877	934902		Nsagana River						
518888	934905		Kutjeni Hill						
518889	934905		Nkutshu						
4311878	934905		Nkutshu Hill						
518890	934908		N’Komazana						
4311879	934908		N’Komazana River						
518891	934910		Mkabana						
4311880	934910		Nkabane						
518892	934912		Nhloya						
4311881	934912		Nhloya River						
518893	934913		Goedgegun						
2929534	934913	link	https://en.wikipedia.org/wiki/Nhlangano						
4311882	934913		Nhlangano						
11760606	934913	fr	Nhlangano						
13800101	934913	unlc	SZNHL						
14944195	934913	wkdt	Q2029702						
518894	934915		Ingwenya						
4311883	934915		Ngwenya						
518895	934916		Ngwempisana River						
4311884	934916		Ngwempisana						
518896	934917		Ngwede						
4311885	934917		Ngwede River						
518905	934920		Mgqongqane						
518906	934920		Ngqongqane Kop						
4311887	934920		Ngqongqane						
2929535	934921	link	https://en.wikipedia.org/wiki/Ngonini						
4311888	934921		Ngonini						
15421520	934921	wkdt	Q15262270						
518907	934923		Ngadwako						
4311889	934923		Ngadwako River						
518908	934924		Ndumbi						
4311890	934924		Ndumbi River						
518909	934925		Ndoban-doba						
4311891	934925		Ndoban-doba River						
714	934927		Mzimpofu						
4311892	934927		Mzimphofu						
518910	934928		Mzimneni						
4311893	934928		Mzimneni River						
518911	934929		Mzimneni						
4311894	934929		Mzimneni River						
518912	934930		Umzimneni River						
518913	934930		Umzimneni						
518914	934930		Mzimneni						
4311895	934930		Mzimneni River						
518915	934931		Mvangalini						
4311896	934931		Mvangatini						
518916	934932		Muweni						
4311897	934932		Muweni River						
518917	934933		Mtombe						
4311898	934933		Mtombe River						
518918	934934		Umtintegwa River						
518919	934934		Umtintegwa						
4311899	934934		Mtindekwa River						
16949223	934934		Mtindekwa						
518920	934935		Mtimpili						
4311900	934935		Mtimpili River						
518921	934936		Mtendekwa						
4311901	934936		Mtendekwa River						
518922	934938		Msuzwane						
4311902	934938		Msuzwane River						
518923	934939		Msulutane						
4311903	934939		Msulutane River						
518924	934942		Mshange Hill						
4311904	934942		Mshange						
518925	934944		Mponono River						
4311905	934944		Mponono						
518926	934945		Mpofu						
4311906	934945		Mpofu River						
518927	934948		Mozane						
4311907	934948		Mozane River						
518932	934950		Mozaankop						
4311909	934950		Mozane						
15421515	934951	wkdt	Q31593207						
15959134	934951		Mouaween Kop						
518933	934952		Motjana						
518934	934952		Motjana River						
4311910	934952		Motjane						
4311911	934955		Mnyame						
518935	934957		Umloseni Kop						
4311912	934957		Mloteni Kop						
15510721	934957	wkdt	Q31593237						
2929536	934959	link	https://en.wikipedia.org/wiki/Mliba						
4311913	934959		Mliba						
15694431	934959	wkdt	Q6885719						
8091251	934960	link	https://en.wikipedia.org/wiki/Mlawula						
8108762	934960		Mlawula						
15461376	934960	wkdt	Q6885679						
518936	934961		Mlaula						
4311914	934961		Mlaula River						
518937	934962		Mkutshane						
518938	934962		Umkutshan						
4311915	934962		Mkutshane River						
518939	934963		Mkumbana						
4311916	934963		Mkumbana River						
16949224	934964		Mhlumeni River						
16949225	934964		Mhlumeni						
2929537	934965	link	https://en.wikipedia.org/wiki/Mhlumeni						
4311917	934965		Mhlumeni						
15636171	934965	wkdt	Q15253288						
2929538	934966	link	https://en.wikipedia.org/wiki/Mhlume						
4311918	934966		Mhlume						
13800098	934966	unlc	SZMHU						
14982260	934966	wkdt	Q2560293						
518940	934967		Mhlotsheni						
2929539	934967	link	https://en.wikipedia.org/wiki/Mhlosheni						
4311919	934967		Mhlosheni						
15675361	934967	wkdt	Q6826879						
518941	934968		Mhlatuze						
4311920	934968		Mhlatuze River						
518942	934969		Mhlatuzane						
4311921	934969		Mhlatuzane River						
518943	934970		Mhlangatane						
4311922	934970		Mhlangatane River						
518944	934971		Mhlambanyati						
2929540	934971	link	https://en.wikipedia.org/wiki/Mhlambanyatsi						
4311923	934971		Mhlambanyatsi						
14987968	934971	wkdt	Q3643138						
518945	934972		Mhlamanti						
4311924	934972		Mhlamanti River						
518946	934974		Mgobodi						
4311925	934974		Mgobodi River						
8092593	934976	link	https://en.wikipedia.org/wiki/Mdimba						
8108763	934976		Mdimba						
15603961	934976	wkdt	Q6802848						
518947	934981		Mbulongwane Kop						
4311926	934981		Mbulungwane Kop						
518948	934982		Mbombwane Kop						
4311927	934982		Mbombane Kop						
15551648	934983	wkdt	Q31593329						
15959135	934983		Mblongeni Mountains						
518949	934984		Mbabane						
4311928	934984		Mbabane River						
6890895	934984	link	https://en.wikipedia.org/wiki/Mbabane_River						
1595832	934985	de	Mbabane						
1595833	934985	en	Mbabane						
1595834	934985	es	Mbabane						
1595835	934985	bg	Мбабане						
1595836	934985	da	Mbabane						
1595837	934985	eo	Mbabano						
1595838	934985	fi	Mbabane						
1595839	934985	fr	Mbabane						
1595840	934985	id	Mbabane						
1595841	934985	io	Mbabane						
1595842	934985	it	Mbabane						
1595843	934985	ja	ムババーネ						
1595844	934985	lt	Mbabanė						
1595845	934985	nl	Mbabane						
1595846	934985	no	Mbabane						
1595847	934985	pl	Mbabane						
1595848	934985	pt	Mbabane						
1595849	934985	sk	Mbabane						
1595850	934985	sr	Мбабане						
1595851	934985	sv	Mbabane						
1635087	934985	mk	Мбабане						
1635088	934985	ru	Мбабане						
1635089	934985	zh	墨巴本						
1649699	934985	el	Μπαμπάνε						
1898821	934985	ca	Mbabane						
1898822	934985	he	מבבנה						
1898823	934985	ko	음바바네						
1898824	934985	sw	Mbabane						
1986057	934985	am	ምባባኔ						
2929541	934985	link	https://en.wikipedia.org/wiki/Mbabane						
3050005	934985	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%B1%D0%B0%D0%B1%D0%B0%D0%BD%D0%B5						
4311929	934985		Mbabane						
7484951	934985	iata	QMN						
9332156	934985	ar	مبابان						
9332157	934985	be	Мбабанэ						
9332158	934985	bo	མ་པ་པན།						
9332159	934985	ce	Мбабане						
9332160	934985	ckb	مبابانێ						
9332162	934985	fa	مبابانه						
9332164	934985	ht	Mbabàn						
9332165	934985	hy	Մբաբանե						
9332166	934985	ka	მბაბანე						
9332167	934985	ky	Мбабане						
9332168	934985	mr	अंबाबाने						
9332171	934985	pa	ਅੰਬਾਬਾਨੇ						
9332172	934985	pnb	مبابین						
9332175	934985	so	Mabane						
9332176	934985	ss	Embabane						
9332179	934985	tg	Мбабане						
9332180	934985	th	อัมบาบาเน						
9332183	934985	udm	Мбабане						
9332184	934985	uk	Мбабане						
9332186	934985	ur	امبابانی						
9332188	934985	yi	מבאבאנע						
11372376	934985	ta	இம்பபான்						
11372377	934985	yue	墨巴本						
13800102	934985	unlc	SZQMN						
15556083	934987	wkdt	Q31593359						
15959136	934987		Mavukulu Kop						
15419890	934988	wkdt	Q31593374						
15959137	934988		Matshemade Kop						
518950	934989		Matapa						
518951	934989		Matsapa						
4311930	934989		Matsapha						
8080466	934989	link	https://en.wikipedia.org/wiki/Matsapha						
8146226	934989	iata	MTS						
8146227	934989	en	Matsapha						
13800097	934989	unlc	SZMAT						
518952	934993		Manziwayo River						
4311931	934993		Manziwayo						
2185648	934994		Manzini	1	1				
2929542	934994	link	https://en.wikipedia.org/wiki/Manzini_District						
3050006	934994	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D0%BD%D0%B7%D0%B8%D0%BD%D0%B8_%28%D0%BE%D0%BA%D1%80%D1%83%D0%B3%29						
5650093	934994		Manzini District						
11760601	934994	fr	Manzini						
13575325	934994		Manzini Region						
14915459	934994	wkdt	Q305395						
518953	934995		Manêini						
518954	934995		Bremersdorp						
1923950	934995	es	Manzini						
1923951	934995	fr	Manzini						
1923952	934995	de	Manzini						
1923953	934995	en	Manzini						
1923954	934995	sr	Манзини						
1923955	934995	sv	Manzini						
1923956	934995	zh-CN	曼齐尼						
2929543	934995	link	https://en.wikipedia.org/wiki/Manzini%2C_Eswatini						
4311932	934995		Manzini						
7482580	934995	iata	MTS						
11497792	934995	el	Μανζίνι						
11497793	934995	he	מנזיני						
11497794	934995	ja	マンジニ						
11497795	934995	ko	만지니						
11497796	934995	lt	Manzinis						
11497797	934995	uk	Манзіні						
11497798	934995	ru	Манзини						
11497799	934995	zh	曼齐尼						
13800100	934995	unlc	SZMTS						
518955	934997		Mantambe						
4311933	934997		Mantambe River						
518956	934998		Mankaiana						
2929544	934998	link	https://en.wikipedia.org/wiki/Mankayane						
4311934	934998		Mankayane						
518959	935000		Madisi Kop						
4311936	935000		Mandisi Kop						
518960	935001		Mancongco Hill						
4311937	935001		Mancongco						
2929545	935002	link	https://en.wikipedia.org/wiki/Maloma						
4311938	935002		Maloma						
14994468	935002	wkdt	Q6744484						
518961	935003		Malolotsha						
4311939	935003		Malolotsha River						
518962	935004		Malobu Kop						
4311940	935004		Malobo Kop						
13800096	935005	unlc	SZMAL						
13826917	935005		Malkerns						
15910899	935005	link	https://en.wikipedia.org/wiki/Malkerns						
518963	935007		Maliaquma Hills						
4311941	935007		Maliaduma Hills						
518964	935009		Malanti Spruit						
4311942	935009		Malanti						
13800099	935014	unlc	SZMLY						
13826918	935014		Mahlanya						
15646970	935016	wkdt	Q31593490						
15959138	935016		Mahamba Mountains						
7071853	935017	link	https://en.wikipedia.org/wiki/Mahamba_%28Swaziland%29						
7118967	935017		Mahamba						
15910895	935021	link	https://en.wikipedia.org/wiki/Madlangempisi						
15959139	935021		Madlangampisi						
518968	935023		Mac Nabs						
518969	935023		Macnab Store						
4311944	935023		Macnab						
518970	935025		Maboko						
4311945	935025		Maboko River						
518971	935027		Mabata						
4311946	935027		Mabata River						
518972	935033		Lutala						
4311947	935033		Lutala River						
518973	935036		Lugula						
4311948	935036		Lugula River						
518974	935037		Qolweni Hills						
4311949	935037		Lugolweni Hills						
518975	935038		Qolweni						
4311950	935038		Lugolweni						
2929546	935040	link	https://en.wikipedia.org/wiki/Lubuli						
4311951	935040		Lubuli						
15360849	935040	wkdt	Q3798826						
518976	935041		Lubugu						
4311952	935041		Lubugu River						
518977	935042		Stegi District						
518978	935042		Lumbombo						
2185649	935042		Lubombo	1	1				
5650094	935042		Lubombo District						
5701152	935042	link	https://en.wikipedia.org/wiki/Lubombo_District						
11760600	935042	fr	Lubombo						
15755493	935042	wkdt	Q856657						
518979	935043		Lubilweni						
4311953	935043		Lubilweni River						
518980	935044		Lomati Store						
4311954	935044		Lomati						
518981	935045		Nomahasha						
4311955	935045		Lomahasha						
7078209	935045	link	https://en.wikipedia.org/wiki/Lomahasha						
13800095	935045	unlc	SZLHH						
15301732	935045	wkdt	Q576512						
518982	935048		Lobomba						
1922408	935048	es	Lobamba						
1922409	935048	pl	Lobamba						
1922410	935048	en	Lobamba						
1922411	935048	he	לובמבה						
1922412	935048	lt	Lobamba						
1922413	935048	pt	Lobamba						
1922414	935048	sr	Лобамба						
2929547	935048	link	https://en.wikipedia.org/wiki/Lobamba						
4311956	935048		Lobamba						
9466682	935048	ar	لوبامبا						
9466683	935048	be	Лабамба						
9466684	935048	bg	Лобамба						
9466685	935048	ckb	لۆبامبا						
9466686	935048	eo	Lobambo						
9466687	935048	fa	لوبامبا						
9466688	935048	ja	ロバンバ						
9466689	935048	ka	ლობამბა						
9466690	935048	ko	로밤바						
9466691	935048	pnb	لوباما						
9466692	935048	uk	Лобамба						
9466693	935048	ur	لوبامبا						
9466694	935048	ru	Лобамба						
9466695	935048	zh	洛班巴						
11415704	935048	yue	洛班巴						
15674041	935050	wkdt	Q31593686						
15959140	935050		Libetse Mountains						
518983	935051		Gollel						
2929548	935051	link	https://en.wikipedia.org/wiki/Lavumisa						
4311957	935051		Lavumisa						
518984	935052		Lapanda Kop						
4311958	935052		Lapande Kop						
518985	935053		Landiwado Kop						
4311959	935053		Landwado Kop						
7076311	935054	link	https://en.wikipedia.org/wiki/Kwaluseni						
7118968	935054		Kwaluseni						
15207460	935054	wkdt	Q3798822						
7071716	935055	link	https://en.wikipedia.org/wiki/Kubuta						
7118969	935055		Kubuta						
15568273	935055	wkdt	Q3432356						
518986	935057		Kopola Spruit						
4311960	935057		Kopola						
518987	935061		Kinkel Kopjes						
4311961	935061		Kinkel Koppies						
518988	935062		Kanyane Kop						
4311962	935062		Kanjane Kop						
518989	935063		Kolweni Hill						
4311963	935063		Kalweni Hill						
518990	935064		Kadake						
4311964	935064		Ka Dake Station						
518991	935065		Josani Kop						
4311965	935065		Josane Kop						
518992	935068		Palata						
4311966	935068		Inyetane						
518993	935070		Inkampeni						
4311967	935070		Inkambeni						
15775681	935071	wkdt	Q31593868						
15959141	935071		Inhlonama Kop						
518994	935072		Imdundu Hill						
4311968	935072		Indundu Hill						
15505866	935073	wkdt	Q31593884						
15959142	935073		Incuba Kop						
15910903	935076	link	https://en.wikipedia.org/wiki/Hluti						
15959143	935076		Hluti						
518995	935080		Hlobane Mission						
4311969	935080		Hlobane						
1926217	935081	es	Hlatikulu						
1926218	935081	en	Hlatikulu						
1926219	935081	sv	Hlatikulu						
2929549	935081	link	https://en.wikipedia.org/wiki/Hlatikulu						
4311970	935081		Hlatikulu						
518996	935082		Hlati Kop						
4311971	935082		Hlati						
518997	935083		Mhlambanyati River						
518998	935083		Mhlambanyati						
518999	935083		Mahlambanyati						
4311972	935083		Hlambanyati River						
16949226	935083		Hlambanyati						
15458306	935084	wkdt	Q31593957						
15959144	935084		Hlabeni Kop						
519000	935085		Pigg’s Peak District						
519001	935085		Piggs District						
2185651	935085		Hhohho	1	1				
5650095	935085		Hhohho District						
5700501	935085	link	https://en.wikipedia.org/wiki/Hhohho_District						
11760599	935085	fr	Hhohho						
13575326	935085		Hhohho Region						
15310167	935085	wkdt	Q735570						
2929550	935086	link	https://en.wikipedia.org/wiki/Herefords						
4311973	935086		Herefords						
15405023	935086	wkdt	Q5738049						
519002	935087		Hawane Kop						
519003	935087		Howan						
4311974	935087		Hawane						
519004	935089		Havelock Mine						
4311975	935089		Havelock						
519005	935090		Grand Valley Estate						
4311976	935090		Granvalley						
7078539	935091	link	https://en.wikipedia.org/wiki/Gege_%28Swaziland%29						
7118970	935091		Gege						
15905705	935091	wkdt	Q3473347						
519006	935092		Gagu						
4311977	935092		Gagu River						
519007	935096		Esulweni						
519008	935096		Ezulweni						
4311978	935096		Ezulwini						
13800094	935096	unlc	SZEZU						
519009	935098		Etshede Kop						
4311979	935098		Esthede Kop						
15492880	935098	wkdt	Q31594006						
519010	935101		Mlembe						
519011	935101		Emlende						
519012	935101		Emlembe Mountain						
4311980	935101		Emlembe						
5713665	935101	link	https://en.wikipedia.org/wiki/Emlembe						
519013	935104		Dubekeni						
4311981	935104		Dubekeni River						
519014	935106		Darktown						
4311982	935106		Darkton						
15578688	935106	wkdt	Q34800481						
519015	935109		Bulungu Mountains						
4311983	935109		Bulunga Mountains						
15224850	935109	wkdt	Q31594066						
519016	935111		Compound						
519017	935111		Emhlembi						
519018	935111		Emlembe						
2929551	935111	link	https://en.wikipedia.org/wiki/Bulembu						
4311984	935111		Bulembu						
519019	935112		Black Umbelusi River						
519020	935112		Black Umbeluzi						
519021	935112		Black Umbuluzi						
519022	935112		Black Mbuluzi						
519023	935112		Black Imbuluzi						
519024	935112		Black Umbelusi						
519025	935112		Black Umbeloosi River						
4311985	935112		Black Umbuluzi River						
2929552	935113	link	https://en.wikipedia.org/wiki/Big_Bend,_Swaziland						
4311986	935113		Big Bend						
15261754	935113	wkdt	Q31915875						
519026	935114		Bunya						
4311987	935114		Bhunya						
15491661	935114	wkdt	Q31915328						
519027	935117		Bambe Inyoni						
4311988	935117		Bambinyoni						
15752839	935117	wkdt	Q31594098						
519028	935118		Balegane Store						
519029	935118		Balegana						
4311989	935118		Balegane						
519030	935119		Amanzilukahla						
4311990	935119		Amanzilukahla River						
519031	935120		Abercorn Pont						
519032	935120		Abercorn Point						
4311991	935120		Abercorn						
1883585	6296968	icao	FDMS						
1888581	6296968	iata	MTS						
5805362	6296968	link	https://en.wikipedia.org/wiki/Matsapha_Airport						
8146225	6296968	en	Manzini - Matsapha Airport						
8087817	7910681	link	https://en.wikipedia.org/wiki/Manzini_North						
11763167	7910682	link	https://en.wikipedia.org/wiki/Siphofaneni						
8078206	7910684	link	https://en.wikipedia.org/wiki/Mkhiweni						
11763221	7910685	link	https://en.wikipedia.org/wiki/Matsanjeni_South						
11839732	7910685		Matsanjeni South						
8075083	7910859	link	https://en.wikipedia.org/wiki/Mayiwane						
11763168	7910860	link	https://en.wikipedia.org/wiki/Lomahasha						
8100915	7910861	link	https://en.wikipedia.org/wiki/Maseyisini						
5713125	7932093	link	https://en.wikipedia.org/wiki/Ngwenya						
8065633	8504610	es	Reserva Natural Mantenga						
8146228	8521372	en	Sikhuphe International Airport						
8146229	8521372	iata	SHO						
8680826	8521372	link	https://en.wikipedia.org/wiki/Sikhuphe_International_Airport						
8793001	8521372	icao	FDSK						
13800103	8521372	unlc	SZSHO						
15910896	9180486	link	https://en.wikipedia.org/wiki/Ntfonjeni						
15561844	11072300	wkdt	Q34879814						
15910897	11127718	link	https://en.wikipedia.org/wiki/Mpholonjeni						
11763188	11127724	link	https://en.wikipedia.org/wiki/Nkhaba						
11763171	11127725	link	https://en.wikipedia.org/wiki/Ngudzeni						
15910898	11127726	link	https://en.wikipedia.org/wiki/Ekukhanyeni						
11712460	11127738	link	https://en.wikipedia.org/wiki/Simunye						
15349070	11127738	wkdt	Q1102993						
11712463	11127739	link	https://en.wikipedia.org/wiki/Mhlangatane						
15698788	11127813	wkdt	Q11709969						
16168661	11127813	link	https://en.wikipedia.org/wiki/Hluthi						
15736014	11127815	wkdt	Q6826831						
15910901	11127816	link	https://en.wikipedia.org/wiki/Mafutseni						
11712571	11127821	link	https://en.wikipedia.org/wiki/Nhlambeni						
11763172	11127975	link	https://en.wikipedia.org/wiki/Ekukhanyeni						
11763182	11127982	link	https://en.wikipedia.org/wiki/Ngwempisi						
11763166	11127989	link	https://en.wikipedia.org/wiki/Hhukwini						
11763195	11127992	link	https://en.wikipedia.org/wiki/Manzini_South						
15792018	11128087	wkdt	Q3798848						
15910893	11128087	link	https://en.wikipedia.org/wiki/Ndzingeni						
16192644	11128090	link	https://en.wikipedia.org/wiki/Mtsambama						
11713043	11128091	link	https://en.wikipedia.org/wiki/Hlane						
15615583	11128092	wkdt	Q3798828						
15910900	11128092	link	https://en.wikipedia.org/wiki/Mahlangatja						
15910894	11128093	link	http://map.immofacts.ch/11128093/mhlangatane.html						
11763185	11128094	link	https://en.wikipedia.org/wiki/Ntondozi						
11713045	11128095	link	https://en.wikipedia.org/wiki/Mpholonjeni						
11713078	11128098	link	https://it.wikipedia.org/wiki/Inkhundla_Mhlume						
11763186	11128345	link	https://en.wikipedia.org/wiki/Nkwene						
11763187	11128346	link	https://en.wikipedia.org/wiki/Mafutseni						
11723100	11128350	link	https://it.wikipedia.org/wiki/Inkhundla_Ludzeludze						
11763198	11128351	link	https://en.wikipedia.org/wiki/Dvokodvweni						
11723021	11128352		Ludzeludze						
11723101	11128352	link	https://it.wikipedia.org/wiki/Inkhundla_Ludzeludze						
16183200	11128352	link	https://en.wikipedia.org/wiki/Ludzeludze						
11763210	11128505	link	https://en.wikipedia.org/wiki/Sigwe						
11763220	11128507	link	https://en.wikipedia.org/wiki/Zombodze						
15616198	11184871	wkdt	Q34870328						
15266684	11184878	wkdt	Q34879292						
16147808	11184879	link	https://en.wikipedia.org/wiki/Sithobela						
16196962	11204891	link	https://en.wikipedia.org/wiki/Lamgabhi						
13636133	11205291	link	https://en.wikipedia.org/wiki/Mpaka						
15581776	11205291	wkdt	Q4167664						
16165691	11205574	link	https://en.wikipedia.org/wiki/Shiselweni_I						
11904878	11237894	link	https://en.wikipedia.org/wiki/Hhelehhele						
15910619	11237894	wkdt	Q5750104						
11904880	11237895	link	https://en.wikipedia.org/wiki/Phophonyane_Falls_Nature_Reserve						
11904889	11237899	link	https://www.google.com/maps/place/Phophonyane+Lodge/@-25.9002356,31.289219,15z/data=!4m5!3m4!1s0x0:0x8e8d60aa8e272fa9!8m2!3d-25.8986558!4d31.2912565						
11937635	11247706	link	https://www.google.com/maps/place/Mangwaneni,+Mbabane,+Swaziland/@-26.3410983,31.1390276,16.25z/data=!4m5!3m4!1s0x1ee8ce9159d4c321:0xdd151c3adeca31d4!8m2!3d-26.3402009!4d31.140968						
11947153	11256967	link	http://swaziland.postcode.info/mambane						
16190796	11257269	link	https://en.wikipedia.org/wiki/Maphalaleni						
11947651	11257278	link	http://heyplaces.co.za/0152422/Hlatsi,_Shiselweni#header_wrapper						
11947667	11257294	link	https://en.wikipedia.org/wiki/Matsanjeni_North						
11947716	11257303	link	https://www.google.com/maps/place/Nsongweni/@-27.0874787,31.1932446,13z/data=!3m1!4b1!4m5!3m4!1s0x1eef0a8ffe2f0f15:0x49b722e4d9388494!8m2!3d-27.0874848!4d31.2282641						
11947774	11257314	link	https://www.google.com/maps/place/Ngculwini,+Swaziland/@-26.5469301,31.479165,623m/data=!3m2!1e3!4b1!4m5!3m4!1s0x1eef5a8daa97818f:0xd322243f21b4926a!8m2!3d-26.5469507!4d31.4824721						
11948005	11257943	link	https://www.google.com/maps/place/Lundzi,+Swaziland/@-26.4448685,30.7333385,11z/data=!3m1!4b1!4m5!3m4!1s0x1ee92a6a85cf1075:0xa87f319f6f14aeae!8m2!3d-26.4449645!4d30.8734294						
16194277	11257943	link	https://en.wikipedia.org/wiki/Lundzi						
11948341	11258149	link	http://isithackday.com/geoplanet-explorer/index.php?woeid=55921320						
15910902	11258149	link	https://en.wikipedia.org/wiki/Mtfongwaneni						
16192880	11523361	link	https://en.wikipedia.org/wiki/Mangcongco						
16339604	12047183	link	https://www.hilton.com/en/hotels/qmngigi-hilton-garden-inn-mbabane/?WT.mc_id=zVSEC0SZ1GI2NaturalSearch3Google_LGHotelListing4DGGeneric_5LocalSearch6QMNGIGI7EN8i81034						
16517050	12123370	link	https://en.wikipedia.org/wiki/Hlane_Royal_National_Park						
16517051	12123370		Hlane Wildlife sanctuary						
