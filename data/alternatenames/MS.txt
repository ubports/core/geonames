16212225	3577977		Woodlands						
16949325	3577983		White River						
16949326	3577983		White						
1455778	3578005		Trants Bay						
4330282	3578005		Trant’s Bay						
1455779	3578024		South Soufriere Hills						
4330283	3578024		South Soufrière Hills						
1455780	3578025		Soufrière						
2030702	3578025	de	Soufriere Hills						
2030703	3578025	es	Soufriere Hills						
2030704	3578025	pl	Soufrière Hills						
2030705	3578025	en	Soufrière Hills						
2030706	3578025	fr	Soufrière						
2030707	3578025	nl	Soufrière						
2030708	3578025	sk	Soufriére Hills						
2030709	3578025	sk	Soufrière Hills						
2986203	3578025	link	https://en.wikipedia.org/wiki/Soufri%C3%A8re_Hills						
3056554	3578025	link	https://ru.wikipedia.org/wiki/%D0%A1%D1%83%D1%84%D1%80%D0%B8%D0%B5%D1%80						
4330284	3578025		Soufrière Hills						
11473810	3578025	cy	Bryniau Soufrière						
11473811	3578025	el	Σουφριέρ Χιλς						
11473812	3578025	he	סופרייר הילס						
11473813	3578025	ja	スーフリエール・ヒルズ						
11473814	3578025	ko	수프리에르힐스						
11473815	3578025	lt	Sufriero kalvų ugnikalnis						
11473816	3578025	ru	Суфриер-Хилс						
11473817	3578025	zh	苏弗里埃尔火山						
16949327	3578034		Sappit River						
16949328	3578034		Sappit						
1455781	3578038		Saint Peter's Village						
2185444	3578039		Saint Peter	1	1				
5650161	3578039		Parish of Saint Peter						
7752467	3578039	link	https://en.wikipedia.org/wiki/Saint_Peter_Parish,_Montserrat						
1455782	3578040		Saint Patrick's Village						
1455783	3578041		Saint John's						
1455784	3578041		Saint John						
1455785	3578043		Saint George Hill						
4330285	3578043		Saint George’s Hill						
1455786	3578044		Saint George						
2185443	3578044		Saint Georges	1	1				
5650162	3578044		Parish of Saint Georges						
7752469	3578044	link	https://en.wikipedia.org/wiki/Saint_Georges_Parish,_Montserrat						
2185442	3578045		Saint Anthony	1	1				
5650163	3578045		Parish of Saint Anthony						
7752468	3578045	link	https://en.wikipedia.org/wiki/Saint_Anthony_Parish,_Montserrat						
1455787	3578054		Roche Point						
1455788	3578054		Roche Bluff						
4330286	3578054		Roche’s Bluff						
1455789	3578062		Rendezvous Village						
4330287	3578062		Rendezvous						
1649355	3578069	de	Plymouth						
1649385	3578069	en	Plymouth						
1649417	3578069	fr	Plymouth						
1649457	3578069	it	Plymouth						
1649518	3578069	da	Plymouth						
1649654	3578069	el	Plymouth						
1649743	3578069	es	Plymouth						
1649780	3578069	fi	Plymouth						
1649816	3578069	pt	Plymouth						
1649851	3578069	sv	Plymouth						
2986204	3578069	link	https://en.wikipedia.org/wiki/Plymouth%2C_Montserrat						
3056555	3578069	link	https://ru.wikipedia.org/wiki/%D0%9F%D0%BB%D0%B8%D0%BC%D1%83%D1%82_%28%D0%9C%D0%BE%D0%BD%D1%82%D1%81%D0%B5%D1%80%D1%80%D0%B0%D1%82%29						
9386697	3578069	ar	بليموث						
9386698	3578069	az	Plimut						
9386699	3578069	be	Плімут						
9386700	3578069	bg	Плимът						
9386701	3578069	eo	Plimuto						
9386702	3578069	he	פלימות						
9386703	3578069	ja	プリマス						
9386704	3578069	ka	პლიმუთი						
9386705	3578069	ko	플리머스						
9386706	3578069	lt	Plimutas						
9386707	3578069	nn	Plymouth på Montserrat						
9386708	3578069	sr	Плимут						
9386709	3578069	th	พลิมัท						
9386710	3578069	uk	Плімут						
9386711	3578069	ur	پلایماؤت، مانٹسریٹ						
9386712	3578069	ru	Плимут						
9386713	3578069	zh	普利茅斯						
11415590	3578069	el	Πλίμουθ						
11415591	3578069	fa	پلی‌موث، مونتسرات						
13787387	3578069	unlc	MSMNI						
13787388	3578069	unlc	MSPLY						
15738724	3578069	wkdt	Q30990						
1455790	3578089		Northwest Bluff						
4330288	3578089		North West Bluff						
16949329	3578092		Nantes River						
16949330	3578092		Nantes						
1455791	3578097		Montserrat Colony of						
1455792	3578097		Montserrat Presidency						
1896646	3578097	pl	Montserrat	1					
1896647	3578097	de	Montserrat	1					
1896648	3578097	en	Montserrat	1					
1896649	3578097	fr	Montserrat	1					
1896650	3578097	es	Montserrat	1					
1896651	3578097	ar	مونتسرات	1					
1896652	3578097	ca	Illa de Montserrat						
1896653	3578097	cs	Montserrat	1					
1896654	3578097	da	Montserrat	1					
1896655	3578097	el	Μοντσερράτ						
1896656	3578097	eo	Moncerato						
1896657	3578097	fi	Montserrat	1					
1896658	3578097	gl	Illa de Montserrat						
1896659	3578097	he	מונטסראט						
1896660	3578097	hr	Montserrat	1					
1896661	3578097	hu	Montserrat	1					
1896662	3578097	id	Montserrat	1					
1896663	3578097	io	Montserrat						
1896664	3578097	is	Montserrat	1					
1896665	3578097	it	Montserrat	1					
1896666	3578097	ja	モントセラト	1					
1896667	3578097	ko	몬트세랫						
1896668	3578097	kw	Montserrat						
1896669	3578097	lt	Montseratas	1					
1896670	3578097	nl	Montserrat	1					
1896671	3578097	nn	Montserrat						
1896672	3578097	no	Montserrat	1					
1896673	3578097	pt	Montserrat	1					
1896674	3578097	ru	Монтсеррат	1					
1896675	3578097	hbs	Montserrat						
1896676	3578097	sk	Montserrat	1					
1896677	3578097	sl	Montserrat	1					
1896678	3578097	sr	Монсерат	1					
1896679	3578097	sv	Montserrat	1					
1896680	3578097	tr	Montserrat	1					
1896681	3578097	zh-CN	蒙特塞拉特						
1982981	3578097	frp	Montsarrat						
1982982	3578097	oc	Montserrat						
1982983	3578097	ro	Montserrat	1					
2420987	3578097	am	ሞንትሴራት	1					
2420988	3578097	be	Мантсерат	1					
2420989	3578097	bg	Монтсерат	1					
2420990	3578097	bn	মন্টসেরাট	1					
2420991	3578097	ca	Montserrat	1					
2420992	3578097	cy	Montserrat	1					
2420993	3578097	dz	མོན་ས་རཊ	1					
2420994	3578097	el	Μονσεράτ	1					
2420995	3578097	et	Montserrat	1					
2420996	3578097	fa	مونت‌سرات	1					
2420997	3578097	ga	Montsarat	1					
2420998	3578097	he	מונסראט	1					
2420999	3578097	hi	मोंटसेरात	1					
2421000	3578097	hr	Montserat	1					
2421001	3578097	ja	モントセラト島	1					
2421002	3578097	ka	მონსერატი	1					
2421003	3578097	ko	몬트세라트	1					
2421004	3578097	lo	ມອນເຊີຣາດ	1					
2421005	3578097	lt	Montserratas	1					
2421006	3578097	lv	Montserrata	1					
2421007	3578097	mk	Монсерат	1					
2421008	3578097	ms	Montserrat	1					
2421009	3578097	mt	Montserrat	1					
2421010	3578097	nb	Montserrat	1					
2421011	3578097	ru	Монсеррат	1					
2421012	3578097	se	Montserrat	1					
2421013	3578097	th	มอนต์เซอร์รัต	1					
2421014	3578097	uk	Монтсеррат	1					
2421015	3578097	zh-TW	蒙塞拉特群岛	1					
2920029	3578097	link	https://en.wikipedia.org/wiki/Montserrat						
3056556	3578097	link	https://ru.wikipedia.org/wiki/%D0%9C%D0%BE%D0%BD%D1%82%D1%81%D0%B5%D1%80%D1%80%D0%B0%D1%82						
4330289	3578097		Montserrat						
7089056	3578097	af	Montserrat	1					
7089057	3578097	ak	Mantserat	1					
7089058	3578097	az	Monserat	1					
7089059	3578097	bm	Moŋsera	1					
7089060	3578097	br	Montserrat	1					
7089061	3578097	bs	Monserat	1					
7089062	3578097	ee	Montserrat nutome	1					
7089063	3578097	eu	Montserrat	1					
7089064	3578097	ff	Monseraat	1					
7089065	3578097	fo	Montserrat	1					
7089066	3578097	gl	Montserrat	1					
7089067	3578097	gu	મોંટસેરાત	1					
7089068	3578097	ha	Manserati	1					
7089069	3578097	ki	Montserrati	1					
7089070	3578097	kl	Montserrat	1					
7089071	3578097	kn	ಮಾಂಟ್‌ಸೆರಟ್	1					
7089072	3578097	lg	Monteseraati	1					
7089073	3578097	ln	Mɔsera	1					
7089074	3578097	lu	Musera	1					
7089075	3578097	mg	Montserrat	1					
7089076	3578097	ml	മൊണ്ടെസരത്ത്	1					
7089077	3578097	mr	मॉन्ट्सेराट	1					
7089078	3578097	nd	Montserrat	1					
7089079	3578097	ne	मोन्टसेर्राट	1					
7089080	3578097	or	ମଣ୍ଟେସେରାଟ୍	1					
7089081	3578097	rm	Montserrat	1					
7089082	3578097	rn	Monteserati	1					
7089083	3578097	sg	Monserâte	1					
7089084	3578097	si	මොන්සෙරාට්	1					
7089085	3578097	sn	Montserrat	1					
7089086	3578097	so	Montserrat	1					
7089087	3578097	sw	Montserrat	1					
7089088	3578097	ta	மாண்ட்செராட்	1					
7089089	3578097	te	మాంట్సెరాట్	1					
7089090	3578097	ti	ሞንትሴራት	1					
7089091	3578097	to	Moʻungaselati	1					
7089092	3578097	ur	مونٹسیراٹ	1					
7089093	3578097	vi	Montserrat	1					
7089094	3578097	yo	Motserati	1					
7089095	3578097	zu	i-Montserrat	1					
8146057	3578097	iata	MNI						
16925804	3578097	zh-Hant	蒙哲臘	1					
16929897	3578097	as	ম’ণ্টছেৰাট	1					
16929898	3578097	ce	Монтсеррат	1					
16929899	3578097	fy	Montserrat	1					
16929900	3578097	gd	Montsarat	1					
16929901	3578097	hy	Մոնսեռատ	1					
16929902	3578097	ig	Montserrat	1					
16929903	3578097	jv	Monsérat	1					
16929904	3578097	kk	Монтсеррат	1					
16929905	3578097	km	ម៉ុងស៊ែរ៉ា	1					
16929906	3578097	ks	مانٹسیراٹ	1					
16929907	3578097	ky	Монтсеррат	1					
16929908	3578097	lb	Montserrat	1					
16929909	3578097	mn	Монтсеррат	1					
16929910	3578097	my	မောင့်စဲရက်	1					
16929911	3578097	pa	ਮੋਂਟਸੇਰਾਤ	1					
16929912	3578097	ps	مانټیسیرت	1					
16929913	3578097	qu	Montserrat	1					
16929914	3578097	sd	مونٽسراٽ	1					
16929915	3578097	sq	Montserat	1					
16929916	3578097	tg	Монтсеррат	1					
16929917	3578097	tk	Monserrat	1					
16929918	3578097	tt	Монтсеррат	1					
16929919	3578097	ug	مونتسېررات	1					
16929920	3578097	uz	Montserrat	1					
16929921	3578097	wo	Mooseraa	1					
16929922	3578097	yi	מאנטסעראַט	1					
16929923	3578097	zh	蒙特塞拉特	1					
16253311	3578098	link	https://en.wikipedia.org/wiki/Montserrat						
16314943	3578098		Montserrat						
16212226	3578099		Monkey						
1455793	3578109		Long Ground						
15268232	3578114	wkdt	Q27753955						
15969142	3578114		Little Redonda						
1455794	3578121		Lees Village						
4330290	3578121		Lee’s						
16949331	3578122		Lee River						
16949332	3578122		Lee						
16949333	3578124		Lawyers River						
16949334	3578124		Lawyers						
1455795	3578127		Kinsale Village						
16949335	3578140		Hot River						
16949336	3578140		Hot						
15577948	3578146	wkdt	Q27753943						
1455796	3578148		Harris Village						
1455797	3578148		Harris' Village						
1455798	3578170		Bransby						
4330291	3578170		Fox’s Bay						
15537494	3578178	wkdt	Q27753933						
16949337	3578181		Farm River						
16949338	3578181		Farm						
1455799	3578185		Farm Village						
4330292	3578185		Farm						
16949339	3578189		Dyer’s River						
16949340	3578189		Dyer’s						
16829387	3578198		Daly River						
16829388	3578198		Daly						
1455800	3578200		Cudjoehead Village						
1455801	3578200		Cuojoe Head						
1455802	3578203		Cork Hill Village						
4330293	3578203		Cork Hill						
15587282	3578204	wkdt	Q27753927						
16829389	3578205		Collins River						
16829390	3578205		Collins						
5903857	3578207	link	https://en.wikipedia.org/wiki/Chances_Peak						
6129021	3578207		Chances Peak						
1455803	3578208		Center Hills						
4330294	3578208		Centre Hills						
1455804	3578216		Carr’s Bay						
4330295	3578216		Cars Bay						
16829391	3578218		Caines River						
16829392	3578218		Caines						
15565891	3578225	wkdt	Q27753914						
15969143	3578225		Breakneck Point						
15446012	3578226	wkdt	Q27753912						
1892999	3578237	icao	TRPM				1		
1893000	3578237	iata	MNI				1		
4330296	3578237	en	Blackburne Airport						
8145449	3578237	link	https://en.wikipedia.org/wiki/W._H._Bramble_Airport						
8146055	3578237	en	W. H. Bramble Airport						
16949341	3578238		Big River						
16949342	3578238		Big						
7144089	7266440	link	https://en.wikipedia.org/wiki/Brades						
8434886	7266440		Brades Estate						
11492754	7266440	az	Breyds						
11492755	7266440	be	Вёска Брэйдс						
11492756	7266440	bg	Брейдс						
11492757	7266440	el	Μπρέιντς						
11492758	7266440	eo	Bradeso						
11492759	7266440	fa	بریدز						
11492760	7266440	he	בראדס						
11492761	7266440	ja	ブレイズ						
11492762	7266440	ka	ბრეიდსი						
11492763	7266440	ko	브레이즈						
11492764	7266440	lt	Breidsas						
11492765	7266440	mk	Брејдс						
11492766	7266440	sr	Брејдс						
11492767	7266440	ta	பிராதெ						
11492768	7266440	th	เบรดส์						
11492769	7266440	uk	Брейдс						
11492770	7266440	ur	بریڈیس						
11492771	7266440	ru	Брейдс						
11492772	7266440	zh	布莱兹						
13787386	7266440	unlc	MSBRD						
8145459	8520849	en	Gerald's Airport				1		
8145460	8520849	iata	MNI						
8145461	8520849	icao	TRPG						
8145634	8520849	link	https://en.wikipedia.org/wiki/John_A._Osborne_Airport						
8146056	8520849	en	John A. Osborne Airport						
15321708	8520849	wkdt	Q1431604						
15755425	10280555	wkdt	Q28403758						
16157487	10280555	link	https://en.wikipedia.org/wiki/National_Museum_of_Montserrat						
