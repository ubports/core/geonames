153496	241123		Findlay						
4313174	241123		Zoroaster Shoal						
153497	241124		Zanguilles Point						
4313175	241124		Pointe Zanguilles						
2921801	241125	link	https://en.wikipedia.org/wiki/Wizard_Reef						
4313176	241125		Wizard Reef						
13524429	241126	en	Wizard Island						
153498	241128	fr	Ile Picard						
4313177	241128	en	West Island						
16144304	241128	link	https://en.wikipedia.org/wiki/Picard_Island						
153499	241129		Vigilant						
4313178	241129		Vigilant Shoal						
153500	241130		Port Victoria						
4313179	241130		Victoria Harbour						
153502	241131		Port Victoria						
1596117	241131	de	Victoria						
1596118	241131	en	Victoria						
1596119	241131	es	Victoria						
1596120	241131	ca	Victòria						
1596121	241131	da	Victoria						
1596122	241131	fi	Victoria						
1596123	241131	fr	Victoria						
1596124	241131	id	Victoria						
1596125	241131	ja	ヴィクトリア						
1596126	241131	ko	빅토리아						
1596127	241131	no	Victoria						
1596128	241131	pl	Victoria						
1596129	241131	pt	Victoria						
1596130	241131	ru	Виктория						
1596131	241131	hbs	Victoria						
1596132	241131	sk	Victoria						
1596133	241131	sv	Victoria						
1630254	241131	lt	Viktorija						
1635921	241131	nl	Victoria						
1649465	241131	it	Victoria						
1649686	241131	el	Βικτόρια						
1900311	241131	am	ቪክቶሪያ፥ ሲሸልስ						
1900312	241131	sw	Victoria						
1900313	241131	vo	Victoria						
1986999	241131	cs	Victoria						
1987000	241131	nov	Viktoria						
1987001	241131	ur	وکٹوریا						
2921802	241131	link	https://en.wikipedia.org/wiki/Victoria%2C_Seychelles						
4313180	241131		Victoria						
8618437	241131		Mahé						
9351136	241131	ar	فيكتوريا						
9351137	241131	arz	فيكتوريا						
9351138	241131	be	Вікторыя						
9351139	241131	bg	Виктория						
9351140	241131	bo	ཝིག་ཐོ་རི་ཡ།						
9351141	241131	ce	Виктори						
9351142	241131	ckb	ڤیکتۆریا، سیشێل						
9351143	241131	cv	Виктори						
9351144	241131	el	Βικτώρια						
9351145	241131	eo	Viktorio						
9351146	241131	fa	ویکتوریا						
9351147	241131	fy	Fiktoria						
9351148	241131	he	ויקטוריה						
9351149	241131	ht	Viktorya						
9351150	241131	hy	Վիկտորիա						
9351151	241131	ka	ვიქტორია						
9351152	241131	lv	Viktorija						
9351153	241131	mk	Викторија						
9351154	241131	mr	व्हिक्टोरिया						
9351155	241131	nn	Victoria på Seychellane						
9351156	241131	oc	Victòria						
9351157	241131	os	Виктори						
9351158	241131	pa	ਵਿਕਟੋਰੀਆ						
9351159	241131	pnb	وکٹوریہ						
9351160	241131	sl	Viktorija						
9351161	241131	so	Fiktoria						
9351162	241131	sr	Викторија						
9351163	241131	th	วิกตอเรีย						
9351164	241131	udm	Виктория						
9351165	241131	uk	Вікторія						
9351166	241131	wo	Wiktoria						
9351167	241131	zh	维多利亚						
11379044	241131	ky	Виктория						
11379045	241131	ta	விக்டோரியா						
11379046	241131	uz	Viktoriya						
11379047	241131	vep	Viktorii						
11379048	241131	yue	維多利亞						
13797790	241131	unlc	SCMAW						
13797791	241131	unlc	SCPOV						
13797794	241131	unlc	SCVIC						
15638422	241131	wkdt	Q3940						
153503	241133		Isle Vache						
153504	241133		Vache Island						
4313181	241133		Île aux Vaches						
15551060	241133	wkdt	Q34790171						
153505	241137		Les Trois Freres						
4313182	241137		Trois Frères						
153506	241138		Les Trois Dames						
4313183	241138		Trois Dames						
153507	241140		La Tortue Rock						
153508	241140	en	Tortue Rock						
4313184	241140		Roche Tortue						
153509	241142		Thérèse Island						
2921803	241142	link	https://en.wikipedia.org/wiki/Th%C3%A9r%C3%A8se_Island						
4313185	241142		Île Thérèse						
15480546	241142	wkdt	Q28687						
153510	241145		Point Matoopa						
4313186	241145		Cape Ternay						
15500852	241145	wkdt	Q34833423						
153511	241146		Port Ternay						
4313187	241146		Baie Ternay						
8422910	241150	link	https://en.wikipedia.org/wiki/Takamaka,_Seychelles						
2187637	241151	nb	Takamaka						
2187638	241151	nn	Takamaka						
2921804	241151	link	https://en.wikipedia.org/wiki/Takamaka%2C_Seychelles						
5650111	241151		Takamaka						
9394531	241151	ro	Districtul Takamaka						
9394532	241151	zh	塔卡瑪卡區						
14911330	241151	wkdt	Q2287379						
153512	241153		Cap Malheureux						
4313188	241153		Pointe du Sud						
13524428	241155	en	Southwest Island						
153513	241156	en	Main Island						
13524435	241156	fr	Grande Terre						
13536125	241157	fr	Île du Sud						
153514	241159		Souris islet						
4313190	241159		Souris Island						
15264141	241162	wkdt	Q31709141						
15959344	241162		Mount Simpson						
153515	241163		Silhouette Island						
2921805	241163	link	https://en.wikipedia.org/wiki/Silhouette_Island						
4313191	241163		Silhouette						
13536852	241164	en	Signal Hill						
153516	241165		Sherard Osborn						
4313192	241165		Sherard Osborn Patch						
153517	241167		Requin						
4313193	241167		Shark Rock						
5713581	241168	link	https://en.wikipedia.org/wiki/Morne_Seychellois						
5722930	241168		Morne Seychellois						
153518	241170		Colony of the Seychelles						
1556131	241170	af	Seychelle	1					
1556132	241170	am	ሲሼልስ	1					
1556133	241170	ar	سيشل	1					
1556134	241170	bg	Сейшели	1					
1556135	241170	ca	Seychelles	1					
1556136	241170	cs	Seychely	1					
1556137	241170	cy	Seychelles	1					
1556138	241170	da	Seychellerne	1					
1556139	241170	de	Seychellen	1					
1556140	241170	el	Σεϋχέλλες	1					
1556141	241170	en	Seychelles	1					
1556142	241170	eo	Sejŝeloj	1					
1556143	241170	es	Seychelles	1					
1556144	241170	et	Seišellid	1					
1556145	241170	eu	Seychelleak	1					
1556146	241170	fa	سیشل	1					
1556147	241170	fi	Seychellit	1					
1556148	241170	fo	Seyskelloyggjar	1					
1556149	241170	fr	Seychelles	1					
1556150	241170	ga	na Séiséil	1					
1556151	241170	he	סיישל						
1556152	241170	hr	Sejšeli	1					
1556153	241170	hu	Seychelles						
1556154	241170	hy	Սեյշելներ	1					
1556155	241170	id	Seychelles	1					
1556156	241170	is	Seychelles-eyjar	1					
1556157	241170	it	Seychelles	1					
1556158	241170	ja	セイシェル						
1556159	241170	ka	სეიშელის კუნძულები	1					
1556160	241170	km	សីស្ហែល	1					
1556161	241170	ko	쉐이쉘						
1556162	241170	lo	ເຊເຊວເລສ	1					
1556163	241170	lt	Seišeliai	1					
1556164	241170	lv	Seišelu salas	1					
1556165	241170	mk	Сејшели	1					
1556166	241170	ms	Seychelles	1					
1556167	241170	mt	is-Seychelles	1					
1556168	241170	nb	Seychellene	1					
1556169	241170	nl	Seychellen	1					
1556170	241170	nn	Seychellene						
1556171	241170	pl	Seszele	1					
1556172	241170	pt	Seychelles						
1556173	241170	ro	Seychelles	1					
1556174	241170	ru	Сейшельские о-ва	1					
1556175	241170	sk	Seychelské ostrovy						
1556176	241170	sl	Sejšeli	1					
1556177	241170	sq	Sejshelle	1					
1556178	241170	sr	Сејшели	1					
1556179	241170	sv	Seychellerna	1					
1556180	241170	sw	Ushelisheli	1					
1556181	241170	th	เซเชลส์	1					
1556182	241170	tr	Seyşeller	1					
1556183	241170	uk	Сейшели						
1556184	241170	vi	Xây-sen						
1556185	241170	zh-TW	塞舌尔群岛	1					
1979969	241170	am	ሲሸልስ						
1979970	241170	an	Seychelles						
1979971	241170	ar	سيشيل						
1979972	241170	ast	Seixeles						
1979973	241170	bg	Сейшелски острови						
1979974	241170	bs	Sejšeli	1					
1979975	241170	frp	Sêch·èles						
1979976	241170	gl	Seixeles - Seychelles						
1979977	241170	hu	Seychelle-szigetek	1					
1979978	241170	io	Seycheli						
1979979	241170	ja	セーシェル	1					
1979980	241170	ko	세이셸	1					
1979981	241170	kw	Seychellys						
1979982	241170	la	Insulae Seisellenses						
1979983	241170	lv	Seišelas						
1979984	241170	nds	Seychellen						
1979985	241170	nn	Seychellane	1					
1979986	241170	no	Seychellene	1					
1979987	241170	oc	Seichèlas						
1979988	241170	pam	Seychelles						
1979989	241170	ps	سیشل						
1979990	241170	ru	Сейшельские Острова	1					
1979991	241170	hbs	Sejšeli						
1979992	241170	sk	Seychely	1					
1979993	241170	sq	Sejshellet						
1979994	241170	sw	Shelisheli						
1979995	241170	tg	Ҷазираҳои Сейшел						
1979996	241170	th	ประเทศเซเชลส์						
1979997	241170	tl	Seyshels						
1979998	241170	tr	Seyşel Adaları						
1979999	241170	ug	سېيشېل ئاراللىرى						
1980000	241170	uk	Сейшельські Острови	1					
1980001	241170	vo	Säceluäns						
2418849	241170	be	Сейшэльскія Астравы	1					
2418850	241170	bg	Сейшелски О-ви	1					
2418851	241170	bn	সিসিলি	1					
2418852	241170	bo	སཱ་ཤཻལ།	1					
2418853	241170	et	Seiiellid	1					
2418854	241170	fa	سیچلز	1					
2418855	241170	gl	Seychelles	1					
2418856	241170	he	איי סיישל	1					
2418857	241170	hi	सेशेल्स	1					
2418858	241170	ia	Seychelles	1					
2418859	241170	ko	세이쉘	1					
2418860	241170	pt	Ilhas Seychelles	1					
2418861	241170	se	Seychellsullot	1					
2418862	241170	tr	Seychelles	1					
2418863	241170	ur	سشلیز	1					
2418864	241170	vi	Xây sen (Seychelles)	1					
2920013	241170	link	https://en.wikipedia.org/wiki/Seychelles						
3047444	241170	link	https://ru.wikipedia.org/wiki/%D0%A1%D0%B5%D0%B9%D1%88%D0%B5%D0%BB%D1%8B						
4313194	241170		Republic of Seychelles						
7090495	241170	ak	Seyhyɛl	1					
7090496	241170	az	Seyşel adaları	1					
7090497	241170	bm	Sesɛli	1					
7090498	241170	br	Sechelez	1					
7090499	241170	ee	Seshɛls nutome	1					
7090500	241170	ff	Seysel	1					
7090501	241170	gu	સેશેલ્સ	1					
7090502	241170	ha	Seychelles	1					
7090503	241170	ki	Shelisheli	1					
7090504	241170	kn	ಸೀಶೆಲ್ಲೆಸ್	1					
7090505	241170	ku	Seyşel	1					
7090506	241170	lg	Sesere	1					
7090507	241170	ln	Sɛshɛlɛ	1					
7090508	241170	lu	Seshele	1					
7090509	241170	mg	Seyshela	1					
7090510	241170	ml	സീഷെൽസ്	1					
7090511	241170	mr	सेशेल्स	1					
7090512	241170	nd	Seychelles	1					
7090513	241170	ne	सेचेलेस	1					
7090514	241170	or	ସେଚେଲସ୍	1					
7090515	241170	rm	Seychellas	1					
7090516	241170	rn	Amazinga ya Seyisheli	1					
7090517	241170	sg	Sëyshêle	1					
7090518	241170	sn	Seychelles	1					
7090519	241170	so	Sishelis	1					
7090520	241170	ta	சீஷெல்ஸ்	1					
7090521	241170	te	సీషెల్స్	1					
7090522	241170	ti	ሲሼልስ	1					
7090523	241170	to	ʻOtumotu Seiseli	1					
7090524	241170	yo	Ṣeṣẹlẹsi	1					
7090525	241170	zu	i-Seychelles	1					
8046217	241170		Seychelles						
11262205	241170	link	http://id.loc.gov/authorities/names/n79071099						
16490093	241170	zh-CN	塞舌尔						
16925847	241170	zh-Hant	塞席爾	1					
16930831	241170	as	ছিচিলিছ	1					
16930832	241170	ce	Сейшелан гӀайренаш	1					
16930833	241170	dz	སེ་ཤཱལས	1					
16930834	241170	fy	Seychellen	1					
16930835	241170	gd	Na h-Eileanan Sheiseall	1					
16930836	241170	ig	Seychelles	1					
16930837	241170	jv	Sésèl	1					
16930838	241170	kk	Сейшель аралдары	1					
16930839	241170	ks	سیشَلِس	1					
16930840	241170	ky	Сейшел аралдары	1					
16930841	241170	lb	Seychellen	1					
16930842	241170	mn	Сейшелийн арлууд	1					
16930843	241170	my	ဆေးရှဲ	1					
16930844	241170	pa	ਸੇਸ਼ਲਸ	1					
16930845	241170	ps	سیچیلیس	1					
16930846	241170	qu	Seychelles	1					
16930847	241170	sd	شي شلز	1					
16930848	241170	si	සීශෙල්ස්	1					
16930849	241170	tg	Сейшел	1					
16930850	241170	tk	Seýşel adalary	1					
16930851	241170	tt	Сейшел утраулары	1					
16930852	241170	ug	سېيشېل	1					
16930853	241170	uz	Seyshel orollari	1					
16930854	241170	wo	Seysel	1					
16930855	241170	yi	סיישעל	1					
16930856	241170	zh	塞舌尔	1					
153520	241171		Seychelles Group						
153521	241171		Seychelles Islands						
4313195	241171		Seychelles						
10224959	241171	sv	Amiranterna						
153522	241172		Point au Sel	1					
4313196	241172		Sel Point						
153523	241174		Bayonne						
4313197	241174		Mount Sebert						
153524	241177		Saint Pierre Island						
4313198	241177		Saint Pierre Islet						
153525	241178		Saint Pierre						
4313199	241178		Saint Pierre Island						
153526	241181		St Louis						
2184629	241181		Saint Louis	1	1				
7530673	241181	link	https://en.wikipedia.org/wiki/Saint_Louis%2C_Seychelles						
9394496	241181	zh	聖路易區						
15759432	241181	wkdt	Q2278695						
153527	241182		Saint Joseph islet						
4313200	241182		Saint Joseph Island						
16756929	241182	link	https://en.wikipedia.org/wiki/St._Joseph_Atoll						
153528	241185		Saint Anne Bay						
153529	241185		Saint Anne’s Bay						
153530	241185		Saint Anns Bay						
4313201	241185		Baie Sainte Anne						
153531	241189		Royal bay						
4313202	241189		Anse Royale						
153532	241191		Round islet						
4313203	241191		Round Island						
153533	241192		Rouge Point						
4313204	241192		Pointe Rouge						
153534	241193		Round Island						
153535	241193		Round islet						
4313205	241193		Île Ronde						
15313175	241193	wkdt	Q34793070						
153536	241196		Ressource islet						
4313206	241196		Ressource Island						
153537	241201		Eagle Island						
153538	241201		Eagle islet						
4313207	241201		Remire Island						
16169253	241201	link	https://en.wikipedia.org/wiki/Remire_Island						
153539	241203		Recif Island						
153540	241203		Recif islet						
4313208	241203		Île aux Récifs						
14868078	241203	wkdt	Q34790155						
153541	241204		Rat islet						
153542	241204		Brulée						
153543	241204		Rat Island						
153544	241204		Brulé Island						
4313209	241204		Île aux Rats						
16204433	241204	link	https://en.wikipedia.org/wiki/Rat_Island_%28Seychelles%29						
153545	241209		Providence Group						
4313210	241209		Atoll de Providence						
15466563	241209	wkdt	Q31892920						
153546	241210	en	Praslin Island						
2024475	241210	de	Praslin						
2024476	241210	fr	Praslin						
2024477	241210	en	Praslin						
2024478	241210	lt	Praslenas						
2024479	241210	nl	Praslin						
2024480	241210	no	Praslin						
2024481	241210	pt	Praslin						
2024482	241210	sv	Praslin						
2921806	241210	link	https://en.wikipedia.org/wiki/Praslin						
4313211	241210		Praslin						
8145952	241210	iata	PRI						
13797792	241210	unlc	SCPRI						
153547	241212		Ile Poule						
153548	241212		Poule Islet						
4313212	241212		Poule Island						
8423517	241214	link	https://en.wikipedia.org/wiki/Port_Glaud						
2921807	241215	link	https://en.wikipedia.org/wiki/Port_Glaud						
5650112	241215		Port Glaud						
15318645	241215	wkdt	Q1808165						
153549	241218		Pointe Police						
4313213	241218		Police Point						
153550	241220		Poivre islets						
4313214	241220		Poivre Islands						
1579293	241221		Pointe La Rue						
2187634	241221	nb	Pointe La Rue						
2187635	241221	nn	Pointe La Rue						
2921808	241221	link	https://en.wikipedia.org/wiki/Pointe_La_Rue						
5650113	241221		Pointe Larue						
14899766	241221	wkdt	Q2877260						
153552	241222		Connan						
4313215	241222		Pointe Conan River						
153553	241223		Platte Island						
2921809	241223	link	https://en.wikipedia.org/wiki/%C3%8Ele_Platte						
4313216	241223		Île Plate						
2187631	241224	nb	Plaisance						
2187632	241224	nn	Plaisance						
2921810	241224	link	https://en.wikipedia.org/wiki/Plaisance%2C_Seychelles						
5650114	241224		Plaisance						
9394494	241224	ro	Districtul Plaisance						
9394495	241224	zh	普萊桑斯區						
14891920	241224	wkdt	Q2199897						
153554	241227		Petit Gouvernment						
4313217	241227		Petit Gouvernement						
153555	241228		West Sister Island						
153556	241228		West Sister						
4313218	241228		Petite Sœur						
16196391	241228	link	https://en.wikipedia.org/wiki/Petite_Soeur						
153557	241230		Petit Paris						
4313219	241230		Petite Paris						
153558	241234		Petit Boileau						
4313220	241234		Petite Boileau						
153559	241235		Anse la Liberte						
4313221	241235		Anse Petite						
153560	241236		Pelican islet						
4313222	241236		Pelican Island						
153561	241238		Piton Jean Marie						
4313223	241238		Oliver Hill						
153562	241241		North Islet						
4313224	241241		North Island						
13536124	241241	fr	Île du Nord						
153563	241242		North islet						
4313225	241242		North Island						
153564	241246		North Island						
4313226	241246		Île du Nord						
6889071	241246	link	https://en.wikipedia.org/wiki/North_Island%2C_Seychelles						
153565	241247		Noody Rock						
4313227	241247		Noddy Rock						
153566	241248		Moyenne islet						
2921811	241248	link	https://en.wikipedia.org/wiki/Moyenne_Island						
4313228	241248		Moyenne Island						
153567	241250		Anse La Mouche						
4313229	241250		Anse á la Mouche						
2187628	241251	nb	Mont Fleuri						
2187629	241251	nn	Mont Fleuri						
2921812	241251	link	https://en.wikipedia.org/wiki/Mont_Fleuri						
5650115	241251		Mont Fleuri						
9393367	241251	ro	Districtul Mont Fleuri						
9393368	241251	zh	蒙弗勒利區						
15358085	241251	wkdt	Q1945413						
2184623	241252		Mont Buxton	1	1				
2298873	241252		buxton						
2921813	241252	link	https://en.wikipedia.org/wiki/Mont_Buxton						
9393369	241252	ro	Districtul Mont Buxton						
9393370	241252	zh	蒙巴克斯頓區						
14899193	241252	wkdt	Q3241682						
15453944	241253	wkdt	Q31709163						
15959345	241253		Mon Plaisir						
153568	241257		La Misere						
4313230	241257		Misere						
153569	241259	en	Malabar Island						
4313231	241259	en	Middle Island						
153570	241260		Mehai Island						
4313232	241260		Menai Island						
153571	241262		Massene Rocks						
153572	241262		Mancienne Rocks						
4313233	241262		Mascene Rocks						
153573	241263		Marie Louise islet						
153574	241263		Marie Louise						
4313234	241263		Marie Louise Island						
8683662	241263	link	https://en.wikipedia.org/wiki/Marie_Louise_Island						
153575	241266		Marie Anne Island						
153576	241266		Mary Anne Island						
4313235	241266		Marianne						
7196534	241266	link	https://en.wikipedia.org/wiki/Marianne_Island						
153577	241268		Pointe Maravi						
4313236	241268		Point Maravi						
153578	241271		Mamelle islet						
153579	241271		Mamelle Island						
4313237	241271		Mamelles						
153580	241272		Anse Jasmin						
4313238	241272		Anse Major						
153581	241273		Chenal Principal						
153582	241273		Great Pass						
153583	241273		Grande Passe						
4313239	241273		Main Channel						
153584	241274		Mahé Island						
2728155	241274	en	Mahé	1					
2728156	241274	fr	Mahé	1					
2728157	241274	ca	Mahé	1					
2728158	241274	de	Mahé	1					
2728159	241274	es	Mahé	1					
2728160	241274	it	Mahé	1					
2729968	241274	en	Mahe						
2921814	241274	link	https://en.wikipedia.org/wiki/Mah%C3%A9%2C_Seychelles						
8066745	241274	iata	SEZ						
8618438	241274		Mahé						
11354389	241274	ar	ماهيه						
11354390	241274	az	Mae						
11354391	241274	bg	Мае						
11354392	241274	el	Μαέ						
11354393	241274	ja	マヘ島						
11354394	241274	ka	მაე						
11354395	241274	ko	마에 섬						
11354396	241274	lt	Mahė						
11354397	241274	lv	Mae						
11354398	241274	ms	Pulau Mahé						
11354399	241274	sr	Махе						
11354400	241274	uk	Мае						
11354401	241274	pt	Ilha de Mahé						
11354402	241274	ru	Маэ						
11354403	241274	zh	马埃岛						
153585	241275		Blanchisseuse Rocks						
4313240	241275		Madge Rocks						
153586	241278		Long islet						
4313241	241278		Long Island						
16139488	241278	link	https://en.wikipedia.org/wiki/Long_Island%2C_Seychelles						
153587	241281		Îlot de L’Islette						
4313242	241281		L’Islette						
16141355	241281	link	https://en.wikipedia.org/wiki/L%27Islette_Island						
153588	241282		L’llot Island						
4313243	241282		L’Îlot						
13536849	241283	fr	Anse Étoile						
153589	241285		The Sisters						
153590	241285		The Sister Islands						
4313244	241285		Les Sœurs						
16200059	241285	link	https://en.wikipedia.org/wiki/Grande_Soeur						
153591	241286		The Wash						
4313245	241286		Les Roches Canales						
153592	241289		Amirante Islands						
153593	241289		Amirante Isles						
153594	241289		the Amirante archipelago						
153595	241289		Amirante Group						
153596	241289		Amirantes						
4313246	241289		Les Amirantes						
5706987	241289	link	https://en.wikipedia.org/wiki/Amirante_Islands						
153597	241295		Point Lazare						
4313247	241295		Pointe Lazare						
153598	241296		Poules Bleu						
4313248	241296		Baie Lazare						
153599	241298		Lascars Point						
153600	241298		Cap Macons						
4313249	241298		Cap Lascars						
15315812	241298	wkdt	Q34828225						
153601	241300		Point La Roe						
4313250	241300		Point La Rue						
1579294	241302		Riviere Anglaise						
1579295	241302		La Rivière Anglaise						
2921815	241302	link	https://en.wikipedia.org/wiki/La_Rivi%C3%A8re_Anglaise						
5650116	241302		English River						
11761518	241302	fr	La Rivière Anglaise						
15726890	241302	wkdt	Q1819963						
153604	241308		La Junon bank						
153605	241308		Junon Bank						
4313251	241308		La Junon						
153606	241310		La Digue Island						
2921816	241310	link	https://en.wikipedia.org/wiki/La_Digue						
4313252	241310		La Digue						
10003312	241310		Digue Island						
1579296	241311		La Digue						
5650117	241311		Inner Islands						
8234757	241311	link	https://en.wikipedia.org/wiki/La_Digue						
153608	241313		Anse de L’ Islette						
153609	241313		Anse Isle						
4313253	241313		Anse l’Islette						
153610	241317		Houdoul Rock						
4313254	241317		Hodoul Rock						
153611	241319		Hermes Shoal						
153612	241319		Hermes Patch						
4313255	241319		Hermes						
13536261	241320	en	Harrison Rock						
13536262	241320	fr	Le Grand Rocher						
15310847	241324	wkdt	Q34912348						
15959346	241324		Haddon Point						
153614	241325		East Sister Island						
153615	241325		East Sister						
4313257	241325		Grand Sœur						
153616	241328		Grande Anse						
4313258	241328		Grand Anse						
1579297	241330		Grand’ Anse						
5650118	241330		Grand Anse Mahe						
5701925	241330	link	https://en.wikipedia.org/wiki/Grand%27Anse_Mah%C3%A9						
1579298	241331		Grand’ Anse						
1579299	241331		Grande Anse						
2187625	241331	nb	Grand’ Anse						
2187626	241331	nn	Grand’ Anse						
5650119	241331		Grand Anse Praslin						
5701926	241331	link	https://en.wikipedia.org/wiki/Grand%27Anse_Praslin						
153620	241333		Pointe Colette						
4313259	241333		Pointe Golette						
153621	241334		Port Glace						
153622	241334		Port Claud						
4313261	241334		Port Glaud						
2187622	241336	nb	Saint Thomas Lowland						
2187623	241336	nn	Saint Thomas Lowland						
2921817	241336	link	https://en.wikipedia.org/wiki/Glacis%2C_Seychelles						
5650120	241336		Glacis						
9394497	241336	ro	Districtul Glacis						
9394498	241336	zh	格拉西斯區						
14912219	241336	wkdt	Q3241696						
153623	241337		Gilberte						
4313262	241337		Gilberte Shoal						
153624	241340	en	Frégate Island						
4313263	241340		Frégate						
8145947	241340	iata	FRK						
8691867	241340	link	https://en.wikipedia.org/wiki/Fr%C3%A9gate_Island						
13797789	241340	unlc	SCFRK						
153625	241342		Fouquet islet						
4313264	241342		Fouquet Island						
153626	241344		Anse Forban						
4313265	241344		Anse Forbans						
2921818	241345	link	https://en.wikipedia.org/wiki/F%C3%A9licit%C3%A9_Island						
4313266	241345		Félicité Island						
153627	241347		Esprit						
4313268	241347		Euphrates Islet						
13524427	241350	en	Northeast Island						
153629	241351		Passe Houareau						
4313270	241351		East Channel						
16949227	241352		Dupuy River						
16949228	241352		Dupuy						
16949229	241354		Du Cap River						
16949230	241354		Du Cap						
153630	241355		Desroches Island						
2921819	241355	link	https://en.wikipedia.org/wiki/Desroches_Island						
4313271	241355		Île Desroches						
13286671	241355	iata	DES						
13797788	241355	unlc	SCDES						
153631	241356		Isle des Noeufs						
153632	241356		Île des Noeufs						
4313272	241356		Île Desnœufs						
153633	241357		De Quincy Village						
4313273	241357		De Quincey Village						
153634	241358		Denis						
153635	241358		Ile Denys						
153636	241358		Dennis Island						
153637	241358		Denis Island						
2921820	241358	link	https://en.wikipedia.org/wiki/Denis_Island						
4313274	241358		Île Denis						
13286669	241358	iata	DEI						
13797787	241358	unlc	SCDEI						
153638	241359		D’Arros islet						
2921821	241359	link	https://en.wikipedia.org/wiki/D%27Arros_Island						
4313275	241359		D’Arros Island						
153639	241360		Curieuse Bay						
4313276	241360		Baie Curieuse						
153640	241361		Curieuse Island						
2921822	241361	link	https://en.wikipedia.org/wiki/Curieuse_Island						
4313277	241361		Curieuse						
153641	241362		The Cousins						
153642	241362		Cousin Islets						
4313278	241362		Cousin Islands						
153643	241363		Cousine Island						
153644	241363		South Cousin Islet						
2921823	241363	link	https://en.wikipedia.org/wiki/Cousine_Island						
4313279	241363		Cousine						
15265393	241363	wkdt	Q375003						
153645	241364		North Cousin Islet						
153646	241364		Cousin Island						
2921824	241364	link	https://en.wikipedia.org/wiki/Cousin_Island						
4313280	241364		Cousin						
153647	241366		Cosmoledo islands						
153648	241366		Cosmoledo Group						
153649	241366		Cosmoledo						
2921825	241366	link	https://en.wikipedia.org/wiki/Cosmoledo						
4313281	241366		Atoll de Cosmoledo						
15381692	241366	wkdt	Q31892906						
153650	241369		Conquest Patch						
153651	241369		Conquest Shoal						
4313282	241369		Conquest						
153652	241371		Concept Island						
2921826	241371	link	https://en.wikipedia.org/wiki/Conception_Island%2C_Seychelles						
4313283	241371		Conception Island						
153653	241372		Connan Point						
4313284	241372		Pointe Conan						
153654	241373		Ile Coetivi						
153655	241373		Coetivy Island						
2921827	241373	link	https://en.wikipedia.org/wiki/Co%C3%ABtivy_Island						
4313285	241373		Coëtivy						
153656	241375		Cocoanut Island						
153657	241375		Michel						
4313286	241375		Coconut Islet						
153658	241379		Chien islet						
4313287	241379		Chien Island						
153659	241380		Chevalier Point						
4313288	241380		Pointe Chevalier						
153660	241381		Chevalier Bay						
4313289	241381		Baie Chevalier						
153661	241382		Chauve Souris						
4313290	241382		Chauve Souris Island						
153662	241383		Chauve Souris islet						
153663	241383		Bat Island						
4313291	241383		Chauve Souris Island						
153664	241386		Cerf Islands						
153665	241386		South Banks						
4313292	241386		Cerf Island						
153666	241387		Île aux Cerfs						
153667	241387	en	Cerf Island						
4313293	241387		Île au Cerf						
7161270	241387	link	https://en.wikipedia.org/wiki/Cerf_Island						
15243847	241387	wkdt	Q28685						
15877424	241388	wkdt	Q31709195						
15959347	241388		Castle Peak						
153668	241389		Piton de Lebouli						
4313294	241389		Castle Peak						
15205834	241389	wkdt	Q31709208						
153669	241391		Cascassaye islet						
4313295	241391		Cascassaye Island						
153670	241394		Cascade Point						
4313296	241394		Pointe Cascade						
8424206	241395	link	https://en.wikipedia.org/wiki/Cascade,_Seychelles						
2184682	241396		Cascade	1	1				
2921828	241396	link	https://en.wikipedia.org/wiki/Cascade%2C_Seychelles						
9394499	241396	ro	Districtul Cascade						
9394500	241396	zh	卡斯喀得區						
15335197	241396	wkdt	Q1928685						
153671	241397		Point Capucin						
153672	241397		Pointe Capacins						
4313297	241397		Pointe Capucins						
13536129	241397	en	Capucin Point						
153673	241400		Caimant Point						
4313298	241400		Pointe Caimant						
153674	241401		Alligator Rock						
4313299	241401		Caiman Rocks						
153675	241402		Caiman						
4313300	241402		Caiman Rock						
153676	241403		Baillon River						
4313301	241403		Caiman River						
16949231	241403		Caiman						
153677	241404		Faon Islet						
153678	241404		Faon Island						
4313302	241404		Île Cachée						
16157306	241404	link	https://en.wikipedia.org/wiki/Cach%C3%A9e_Island						
16949232	241410		Bougainville River						
16949233	241410		Bougainville						
10262565	241413	link	https://en.wikipedia.org/wiki/Boudeuse_Island						
153679	241414		Booby islet						
4313303	241414		Booby Island						
16185218	241414	link	https://en.wikipedia.org/wiki/Booby_Island%2C_Seychelles						
153680	241419	en	Sea Cow island						
153682	241419	fr	Ile Vaches de Mer						
2921829	241419	link	https://en.wikipedia.org/wiki/Bird_Island%2C_Seychelles						
4313304	241419	en	Bird Island	1					
13535091	241419	fr	Ile aux Vaches						
13636001	241419	iata	BDI						
13636002	241419	fr	Île aux Oiseaux	1					
13797786	241419	unlc	SCBDI						
15416132	241420	wkdt	Q31915951						
15959348	241420		Bijoutier Island						
153683	241422		Benjamen islet						
4313305	241422		Benjamen Island						
15228203	241422	wkdt	Q31914152						
153684	241423		Belle Ombre						
4313306	241423		Bel Ombre						
8424518	241423	link	https://en.wikipedia.org/wiki/Bel_Ombre,_Seychelles						
1579300	241424		Belombre						
2187619	241424	nb	Saint Paul Charlestown						
2187620	241424	nn	Saint Paul Charlestown						
2921830	241424	link	https://en.wikipedia.org/wiki/Bel_Ombre%2C_Seychelles						
5650121	241424		Bel Ombre						
9394995	241424	ro	Districtul Bel Ombre						
9394996	241424	zh	貝爾翁布雷區						
15734690	241424	wkdt	Q3245439						
2184676	241426		Bel Air	1	1				
2921831	241426	link	https://en.wikipedia.org/wiki/Bel_Air%2C_Seychelles						
9394122	241426	ro	Districtul Bel Air						
9394123	241426	zh	貝爾艾爾區						
14898895	241426	wkdt	Q815077						
4313307	241427		Beau Vallon						
8409966	241427	link	https://en.wikipedia.org/wiki/Beau_Vallon,_Seychelles						
2187616	241428	nb	Saint Mary Cayon						
2187617	241428	nn	Saint Mary Cayon						
2921832	241428	link	https://en.wikipedia.org/wiki/Beau_Vallon						
5650122	241428		Beau Vallon						
15756619	241428	wkdt	Q7479348						
153686	241429	fr	Seche Island						
153687	241429		Beacon islet						
153688	241429		Balise						
4313308	241429		Beacon Island						
13536818	241429		Harrison Rock						
16949234	241434		Barbarons River						
16949235	241434		Barbarons						
2184677	241438		Baie Sainte Anne	1	1				
2921833	241438	link	https://en.wikipedia.org/wiki/Baie_Sainte_Anne						
9393374	241438	fr	Baie Sainte-Anne						
9393375	241438	ro	Districtul Baie Sainte Anne						
9393376	241438	zh	貝聖安那區						
14888042	241438	wkdt	Q803716						
2187613	241439	nb	Saint John Capesterre						
2187614	241439	nn	Saint John Capesterre						
2921834	241439	link	https://en.wikipedia.org/wiki/Baie_Lazare						
5650123	241439		Baie Lazare						
9395487	241439	ro	Districtul Baie Lazare						
9395488	241439	zh	貝拉扎爾區						
14851042	241439	wkdt	Q2494551						
153689	241440		Assumption Island						
2921835	241440	link	https://en.wikipedia.org/wiki/Assumption_Island						
4313310	241440		Assumption						
153690	241441		Aride Island						
2921836	241441	link	https://en.wikipedia.org/wiki/Aride_Island						
4313311	241441		Île Aride						
153691	241443		Anse Royal						
4313312	241443		Anse Royale						
8422809	241443	link	https://en.wikipedia.org/wiki/Anse_Royale						
2187610	241444	nb	Saint James Windward						
2187611	241444	nn	Saint James Windward						
2921837	241444	link	https://en.wikipedia.org/wiki/Anse_Royale						
5650124	241444		Anse Royale						
14899219	241444	wkdt	Q3241674						
2187604	241447	nb	Saint George Basseterre						
2187605	241447	nn	Saint George Basseterre						
2921838	241447	link	https://en.wikipedia.org/wiki/Anse_Etoile						
5650125	241447		Anse Etoile						
9395493	241447	fr	Anse Étoile						
9395494	241447	ro	Districtul Anse Etoile						
9395495	241447	zh	安塞艾托瓦區						
14912629	241447	wkdt	Q387293						
8423110	241448	link	https://en.wikipedia.org/wiki/Anse_Boileau						
2187601	241449	nb	Saint Anne Sandy Point						
2187602	241449	nn	Saint Anne Sandy Point						
2921839	241449	link	https://en.wikipedia.org/wiki/Anse_Boileau						
5650126	241449		Anse Boileau						
15737422	241449	wkdt	Q569453						
2187598	241450	nb	Christ Church Nichola Town						
2187599	241450	nn	Christ Church Nichola Town						
2921840	241450	link	https://en.wikipedia.org/wiki/Anse-aux-Pins						
5650127	241450		Anse aux Pins						
9393965	241450	en	Anse-aux-Pins						
9393966	241450	ms	Anse-aux-Pins						
9393967	241450	ro	Districtul Anse aux Pins						
9393968	241450	zh	安塞奧潘區						
14915799	241450	wkdt	Q569458						
153692	241451		Annonyme Island						
153693	241451		Anonyme Islet						
153694	241451		Anonyme Island						
4313313	241451		Île Anonyme						
7539798	241451	link	https://en.wikipedia.org/wiki/Anonyme_Island						
15497956	241451	wkdt	Q4066804						
153695	241454		Alphonse Island						
4313314	241454		Île Alphonse						
15264698	241454	wkdt	Q34790035						
153696	241455	en	Aldabra Islands						
2921841	241455	link	https://en.wikipedia.org/wiki/Aldabra_Group						
4313315	241455	fr	Groupe d’Aldabra						
153698	241457	fr	Îles Africaines						
153699	241457		African islets						
4313316	241457		African Islands						
13536126	241457	en	African Banks						
13536127	241457	fr	Îlots Africains						
13536128	241457	link	https://en.wikipedia.org/wiki/African_Banks						
2923532	448408	link	https://en.wikipedia.org/wiki/Les_Mamelles						
5651674	448408		Les Mamelles						
9393365	448408	ro	Districtul Les Mamelles						
9393366	448408	zh	雷瑪麥爾區						
15696612	448408	wkdt	Q2280357						
2923533	448409	link	https://en.wikipedia.org/wiki/Roche_Caiman						
5651675	448409		Roche Caiman						
9393371	448409	fr	Roche Caïman						
9393372	448409	ro	Districtul Roche Caiman						
9393373	448409	zh	羅切凱曼區						
14899247	448409	wkdt	Q2718315						
2923534	448410	link	https://en.wikipedia.org/wiki/Au_Cap						
5651676	448410		Au Cap						
9395490	448410	ro	Districtul Au Cap						
9395492	448410	zh	奧凱普區						
14912850	448410	wkdt	Q2667004						
519327	936198		Göelette islet						
4313260	936198		Goelette Island						
519328	936199		Joâo de Nova						
519329	936199		Farquhar Group						
519330	936199		Farquhar Islands						
2929572	936199	link	https://en.wikipedia.org/wiki/Farquhar_Group						
4313267	936199		Atoll de Farquhar						
2929573	936200	link	https://en.wikipedia.org/wiki/Astove_Island						
4313309	936200		Astove Island						
1883711	6297094	icao	FSIA						
1888679	6297094	iata	SEZ						
5883651	6297094	link	https://en.wikipedia.org/wiki/Seychelles_International_Airport						
8145948	6297094	en	Seychelles International Airport						
8145949	6297094	fr	Aéroport International des Seychelles						
13797793	6297094	unlc	SCSEZ						
15426657	6297094	wkdt	Q1432843						
2038835	6354923	iata	BDI						
5887361	6354923	icao	FSSB						
5892242	6354923	fr	Aéroport de l’île aux Oiseaux						
5892243	6354923	en	Bird Island Airport						
6895537	6354923	link	https://en.wikipedia.org/wiki/Bird_Island_Airport						
2038837	6354925	iata	DEI						
5887758	6354925	icao	FSSD						
5892119	6354925		Denis Island						
5892120	6354925		Denis Island Airport	1					
7490270	6354925	link	https://en.wikipedia.org/wiki/Denis_Island_Airport						
5762965	6354926	link	https://en.wikipedia.org/wiki/Desroches_Airport						
5804482	6354926	icao	FSDR						
5804615	6354926	iata	DES						
5891549	6354926		Desroches						
5891550	6354926		Desroches Airport	1					
2080543	6453411	iata	PRI						
2080544	6453411	icao	FSPP						
3033704	6453411	link	https://en.wikipedia.org/wiki/Praslin_Island_Airport						
5892643	6453411	en	Praslin Island						
5892644	6453411	en	Praslin Island Airport	1					
5893274	6453411	pl	Port lotniczy Praslin						
8145950	6453411	fr	Aéroport de Praslin						
8145951	6453411	ru	Аэропорт острова Праслен						
15577306	6453411	wkdt	Q4073707						
13188612	6492679	link	https://www.hilton.com/en/hotels/sezhihi-hilton-seychelles-northolme-resort-and-spa/?SEO_id=OTHR-EMEA-HI-SEZHIHI						
15335834	6492679	wkdt	Q52174600						
13797785	6691844	unlc	SCANS						
7136076	8298990	link	https://en.wikipedia.org/wiki/Fr%C3%A9gate_Island_Airport						
7635768	8298990	iata	FRK						
8145422	8298990	icao	FSSF						
8145945	8298990	en	Frégate Island Airport						
8145946	8298990	fr	Aéroport de Frégate Island						
8237691	8533711	link	https://en.wikipedia.org/wiki/Alphonse_Airport						
8237692	8533711	icao	FSAL						
8247054	8533828	link	https://en.wikipedia.org/wiki/Farquhar_Airport						
8247056	8533828	icao	FSFA						
8247204	8533840	link	https://en.wikipedia.org/wiki/Assumption_Island_Airport						
8247205	8533840	icao	FSAS						
8247206	8533841	link	https://en.wikipedia.org/wiki/Co%C3%ABtivy_Airport						
8247207	8533841	icao	FSSC						
8288287	8555587	link	https://en.wikipedia.org/wiki/Platte_Island_Airport						
8288288	8555587	icao	FSPL						
8289063	8555777	link	https://en.wikipedia.org/wiki/Marie_Louise_Island_Airport						
8289064	8555777	icao	FSMA						
8289089	8555784	link	https://en.wikipedia.org/wiki/Remire_Island_Airport						
8289090	8555784	icao	FSSR						
8289111	8555795	link	https://en.wikipedia.org/wiki/D%27Arros_Island_Airport						
8289112	8555795	icao	FSDA						
16182345	8581502	link	https://en.wikipedia.org/wiki/Anse_Lazio						
13264516	10063083	link	http://www3.hilton.com/en/hotels/seychelles/hilton-seychelles-labriz-resort-and-spa-SEZLBHI/index.html?WT.mc_id=zELWAKN0EMEA1HI2DMH3LocalSearch4DGGenericx6SEZLBHI						
13264875	10114884	link	https://www.hilton.com/en/hotels/sezdtdi-doubletree-seychelles-allamanda-resort-and-spa/?WT.mc_id=zELWAKN0EMEA1DT2DMH3LocalSearch4DGGenericx6SEZDTDI						
16339877	10629843	de	Silhouette						
16199454	11280665	link	https://en.wikipedia.org/wiki/University_of_Seychelles						
13523838	11791455	de	Aldabra						
13523839	11791455	en	Aldabra						
13523840	11791455	es	Aldabra						
13523841	11791455	lt	Aldabra						
13523842	11791455	nl	Aldabra						
13523843	11791455	pl	Aldabra						
13523844	11791455	pt	Aldabra						
13523845	11791455	sv	Aldabra						
13523846	11791455	fr	Aldabran						
13523848	11791455	no	Aldabraøya						
13523849	11791455	gl	Atol Aldabra						
13523850	11791455	sr	Алдабра						
13523851	11791455	ru	Альдабра						
13523852	11791455	link	https://en.wikipedia.org/wiki/Aldabra						
13523853	11791455	en	Aldabra Island				1		
13527568	11791458	en	West Point				1		
13524430	11791490	en	South-East Passage						
13524433	11791502	en	Johnny Channel						
16206150	11876017	link	https://en.wikipedia.org/wiki/Outer_Islands_(Seychelles)						
17027121	12324225	link	https://www.hilton.com/en/hotels/sezitol-mango-house-seychelles/?SEO_id=OTHR-EMEA-OL-SEZITOL						
