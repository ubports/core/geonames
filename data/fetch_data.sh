#!/bin/bash

rm *.txt

wget http://download.geonames.org/export/dump/admin1CodesASCII.txt
wget http://download.geonames.org/export/dump/cities15000.zip
wget http://download.geonames.org/export/dump/countryInfo.txt

unzip cities15000.zip
rm *.zip

python3 fetch_alternative_names.py
rm alternativenames/readme.txt
