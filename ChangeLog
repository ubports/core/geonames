2024-01-25 Mike Gabriel

        * Release 0.3.1 (HEAD -> main, tag: 0.3.1)

2023-12-17 Mike Gabriel

        * Merge branch 'add/more-gnuinstalldirs-usage' into 'main' (ec91987)

2023-09-01 OPNA2608

        * demo/CMakeLists.txt: Use CMAKE_INSTALL_BINDIR for install location
          (3bca6d4)

2023-01-04 Mike Gabriel

        * Release 0.3.0 (73c5dfc) (tag: 0.3.0)

2022-10-03 Mike Gabriel

        * Merge branch 'personal/sunweaver/no-bzr-anymore' into 'main'
          (298997b)
        * .bzr-builddeb/default.conf: Drop file. (3a88b84)
        * Merge branch 'personal/mariogrip/alt-split' into 'main' (e3e713b)
        * cmake/FindGtkDoc.cmake: Drop debug messages. (6d6b137)
        * CMakeLists.txt: Bump upstream version to 0.3.0. (c1d90e3)
        * debian/rules: Use --parallel as DH build option. Seems to work now.
          (f408d34)
        * doc/reference/CMakeLists.txt: Use LIBRARIES instead of LDFLAGS in
          gtk_doc_add_module(). (2d739c3)
        * src/CMakeLists.txt: Drop duplicate target_link_libraries() call
          with explicit '-lglib-2.0'. (91ef63f)
        * cmake/FindGtkDoc.cmake: Prepend '-l' to library target names when
          populating ${ldflags}. (783431e)
        * {doc/reference/CMakeLists.txt,cmake/FindGtkDoc.cmake}: Less hacky
          approach of handling empty geonames.types file. (cabdba7)
        * cmake/FindGtkDoc.cmake: Explicitly
          CMAKE_FIND_LIBRARY_{PREFIXES,SUFFIXES}. (8893fa3)

2022-07-15 Marius Gripsgard

        * cmake: Use newer local copy for GtkDoc (509a97e)

2022-05-25 Marius Gripsgard

        * cmake: Fix ninja by not depending on files with same name as target
          (bb10187)
        * cmake: Add back targets and dependencies on the main library
          (70d47a6)

2022-05-24 Marius Gripsgard

        * cruft: Remove leftover automake files (5e9d988)
        * Add cmake patches from @deathmist (d26eecb)
        * cmake: Fix parallel building (d840500)

2022-03-04 Marius Gripsgard

        * data/fetch_data.sh: Remove before fetching new data (2f4715c)
        * tests: Update tests to mach new data (03-04-2022) (f8221da)
        * data: Update data (03-04-2022) (92d76c5)
        * data: Add alternate names (10-01-2022) (9d43c1c)
        * src: Add support for alternate names in seperate files (b29b7ff)
        * data: Rename admin1Codes file to match data src (7883c2f)
        * tests: Update tests to mach new data (10-01-22) (b24e713)
        * data: Update geonames data (10-01-2022) (dcb1983)

2022-03-07 Marius Gripsgard

        * src: Fixup pkgconfig includedir (a96013c)

2022-03-05 Marius Gripsgard

        * debian/ src/: Fixup copyright (1585382)
        * debian: Fixup pkg to mach debian (f6f8d65)

2022-03-04 Marius Gripsgard

        * debian/changelog: Add new development entry (bbd3404)
        * debian: Update to support cmake (5d48f3d)

2022-03-03 Marius Gripsgard

        * Switch from automake to cmake (57f63b9)

2022-07-14 Marius Gripsgard

        * Add build for ci & bump to a cleaner version (af3eeb1)

2021-11-30 Marius Gripsgard

        * [src] Rename com/ubuntu namespace to com/lomiri (6c6f06b)

2019-02-14 Jelmer Vernooĳ

        * Apply janitor changes. (1879368)

2017-02-20 Bileto Bot

        * Releasing 0.2+17.04.20170220-0ubuntu1 (ecbddd9)

2017-02-20 Albert Astals Cid

        * Be less strict on city search (a7e88ea)

2017-02-01 Albert Astals Cid

        * Be less strict on city search (bea35a0)

2016-04-26 CI Train Bot

        * Releasing 0.2+16.04.20160426-0ubuntu1 (b8b353b)

2016-04-26 Michael Terry

        * Add support for translations of city, states, and country names.
          Approved by: Josh Arenson (484bcda)

2016-04-20 Michael Terry

        * Nope, not fixed yet (07fdaee)
        * try removing s390x workaround, now that bug is claimed fixed
          (c0a2423)
        * remove unused variables (44b03bc)
        * remove Authors section from copyright headers; not strictly needed
          and annoying to update (9adf9c6)

2016-04-12 Michael Terry

        * Fix typo (b2186b6)
        * Try to fix s390x failure another way (1c9ec76)

2016-04-05 Michael Terry

        * Remove some unneeded changes (741f701)
        * Add language-pack-en to Build-Deps for tests on s390x (b7764a2)

2016-04-04 Michael Terry

        * Print test results on a failure (93fbc5c)
        * Build-Depen on locales (26568de)
        * Add missing unref (6274a27)
        * Fix up translation support to generate and ship po files (c3f52ac)
        * Update geonames data (fc29666)

2016-03-29 Michael Terry

        * Some fixes, make sure zh-TW shows up in mkdb (bbd1a9f)

2016-03-25 Michael Terry

        * Merge from trunk (3ae8c09)

2016-03-21 CI Train Bot

        * Releasing 0.2+16.04.20160321-0ubuntu1 (327990e)

2016-03-21 Michael Terry

        * Add geonames_city_get_country_code, geonames_city_get_latitude,
          geonames_city_get_longitude, and
          geonames_city_get_population. Approved by: Nick Dedekind,
          Allison Ryan Lortie (d8625f1)
        * And for safety, initialize static geonames_data to NULL (7c9e4e6)

2016-03-15 CI Train Bot

        * Releasing 0.1+16.04.20160315-0ubuntu1 (a158578)

2016-03-15 Michael Terry

        * Fix double-free when querying. Approved by: Michał Sawicz (91e3c20)
        * Some packaging cleanups, making the package ready for the citrain.
          Approved by: Nick Dedekind (68bb7a4)

2016-03-09 Michael Terry

        * Clean up locale handling slightly (80a2996)

2016-03-08 Michael Terry

        * Only search English and current-language translations, for speed
          (771903f)
        * Add support for translated location names (481b337)

2016-03-01 Michael Terry

        * Merge expose-more (7cc2730)
        * drop cast (2ce30e8)

2016-02-29 Michael Terry

        * And add symbol; geeze (a5f6d7a)
        * Mention it in changelog (3005f01)
        * Add get_population (0e869c7)
        * double free (fbc7911)

2016-02-26 Michael Terry

        * Merge trainify (756c921)
        * use G_BEGIN_DECLS (1b2ba2c)
        * Merge trainify (19b8aeb)
        * Lower gtk-doc requirement to match what vivid has (7d698b5)
        * Expose more info (5c240d4)
        * Clean up packaging (c55053e)
        * Fix double-free when querying (36de23a)
        * Merge debian packaging branch (6c1809e)

2015-12-10 Sebastien Bacher

        * don't install the mkdb binary and set dev multiarch info (8015fdd)
        * use dh_install --list-missing (49cb9cc)
        * include the CC-BY-SA license in the debian/copyright (ed332aa)
        * list geonames.org copyright (a651258)
        * the dev depends on the library (537d8a5)

2015-12-09 Lars Uebernickel

        * Merge upstream (0340793)
        * Don't install geonames-mkdb (4b6dab2)
        * Dist COPYING and autogen.sh (b417a4f)

2015-12-09 Sebastien Bacher

        * Add license text (de7d606)
        * Update symbols and packaging format, ship documentation and tools
          (73aa37c)

2015-12-09 Iain Lane

        * Add gtk-doc-tools, pkg-config and libglib2.0-dev BDs required for
          building (8bb310f)

2015-12-09 Sebastien Bacher

        * debian: minor packaging fixes (eef0ceb)

2015-12-09 Lars Uebernickel

        * debian: add copyright information for files from geonames.org
          (f884306)
        * debian: add detailed package descriptions (58d55d7)
        * debian: change library name (aea58d6)
        * Add debian packaging directory (269034e)

2015-10-07 Lars Uebernickel

        * Add documentation (748e2a3)

2015-10-05 Lars Uebernickel

        * Add sanity test for empty results (ec7aa0b)
        * Add support for g_autoptr (530c49a)
        * tests: link against libgeonames and use new sync api (dd747f5)
        * Add geonames_query_cities_sync() (502abd7)
        * Add geonames_get_n_cities() (524e26d)
        * Empty argument list is bad C99 (323f373)
        * demo: don't leak indices (c10c712)
        * query_cities: return array directly (0ed1bdc)

2015-09-15 Lars Uebernickel

        * Favor larger cities (dc12fb0)
        * Add demo application (636a7e7)

2015-09-14 Lars Uebernickel

        * query_cities_finish: write to indices out parameter (de8ed73)
        * Ensure that geonames db is loaded before querying it (4037f29)
        * Fix typo in public enum field (f5cc180)
        * Install headers (24f5ad4)
        * Add pkgconfig file (c644626)
        * Initial commit (3f56f16)
